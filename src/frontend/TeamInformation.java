/**
 * ---------------------------------------------------------------------------
 * File name: TeamInformation.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package frontend;

import java.util.ArrayList;
import java.util.HashMap;
import cfbgame.RivalryList;
import cfbgame.Team;
import cfbgame.TeamList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class TeamInformation
{
	private TeamList teamList;
	private RivalryList rivalryList;
	
	public TeamInformation(TeamList teamList, RivalryList rivalryList)
	{
		this.teamList = teamList;
		this.rivalryList = rivalryList;
	}
	
	/**
	 * @return teamList
	 */
	public ArrayList <Team> getTeamList ( )
	{
		return teamList.getTeamList ( );
	}
	
	/**
	 * @return primaryRivalryMap
	 */
	public HashMap <Team, Team> getPrimaryRivalryMap ( )
	{
		return rivalryList.getPrimaryRivalryMap ( );
	}
	
}
