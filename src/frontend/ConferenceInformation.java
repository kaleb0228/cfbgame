/**
 * ---------------------------------------------------------------------------
 * File name: ConferenceInformation.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package frontend;

import java.util.ArrayList;
import cfbgame.Conference;
import cfbgame.ConferenceList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class ConferenceInformation
{
	private ConferenceList confList;
	
	public ConferenceInformation(ConferenceList confList)
	{
		this.confList = confList;
	}
	
	public ArrayList<Conference> getConfList()
	{
		return confList.getConfList ( );
	}
}
