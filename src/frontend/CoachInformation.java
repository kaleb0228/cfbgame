/**
 * ---------------------------------------------------------------------------
 * File name: CoachInformation.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package frontend;

import java.util.ArrayList;
import java.util.HashMap;
import cfbgame.*;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class CoachInformation
{
	private HeadCoachList hcList;
	private OffCoordinatorList ocList;
	private DefCoordinatorList dcList;
	private CoachAssignment coachAssignment;
	
	public CoachInformation(HeadCoachList hcList, OffCoordinatorList ocList, DefCoordinatorList dcList, CoachAssignment coachAssignment)
	{
		this.hcList = hcList;
		this.ocList = ocList;
		this.dcList = dcList;
		this.coachAssignment = coachAssignment;
	}
	
	public HashMap<Team, HeadCoach> getHeadCoachMap()
	{
		return coachAssignment.getTeamHcList ( );
	}
	
	public HashMap<Team, OffCoordinator> getOcMap()
	{
		return coachAssignment.getTeamOcList ( );
	}
	
	public HashMap<Team, DefCoordinator> getDcMap()
	{
		return coachAssignment.getTeamDcList ( );
	}
	
	public ArrayList<HeadCoach> getHcList()
	{
		return hcList.getHcList ( );
	}
	
	public ArrayList<OffCoordinator> getOcList()
	{
		return ocList.getOcList ( );
	}
	
	public ArrayList<DefCoordinator> getDcList()
	{
		return dcList.getDcList ( );
	}
	
	
}
