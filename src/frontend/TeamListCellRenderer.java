/**
 * ---------------------------------------------------------------------------
 * File name: TeamListCellRenderer.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 18, 2020
 * ---------------------------------------------------------------------------
 */

package frontend;

import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import cfbgame.Team;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 18, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class TeamListCellRenderer extends DefaultListCellRenderer
{
	private static final long serialVersionUID = 1L;
	
	@Override
	public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus)
	{
		if(value instanceof Team)
		{
			value = ((Team)value).getName ( );
		}
		return super.getListCellRendererComponent (list, value, index, isSelected, cellHasFocus);
	}
}
