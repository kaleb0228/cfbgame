/**
 * ---------------------------------------------------------------------------
 * File name: TeamInfoPanel.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 17, 2020
 * ---------------------------------------------------------------------------
 */

package frontend;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import cfbgame.Coach;
import cfbgame.Conference;
import cfbgame.DefCoordinator;
import cfbgame.HeadCoach;
import cfbgame.OffCoordinator;
import cfbgame.Team;


/**
 * Build the information panel that is displayed in the game introduction menu, with info about the team being chosen.
 *
 * <hr>
 * Date created: Nov 17, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class TeamInfoPanel extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	private JLabel teamLabel;
	private JLabel teamRatingLabel;
	private JLabel conferenceLabel;
	private JLabel stadiumNameLabel;
	private JLabel locLabel;
	private JLabel rivalLabel;	
	private JLabel hcLabel;
	private JLabel ocLabel;
	private JLabel dcLabel;
	
	
	private Team selectedTeam;
	private Team rivalTeam;
	private HeadCoach selectedCoach;
	private Coach selectedOc;
	private Coach selectedDc;
	
	private JPanel coachInfo;
	private JPanel teamInfo;
	private JPanel schoolInfo;
	
	private EtchedBorder panelBorder;
	
	private TitledBorder infoTitle = BorderFactory.createTitledBorder ("Team Information");
	private TitledBorder coachTitle = BorderFactory.createTitledBorder ("Coaching Staff");
	private TitledBorder schoolTitle = BorderFactory.createTitledBorder ("School Information");
	
	private TeamInformation ti;
	private ConferenceInformation confInfo;
	private CoachInformation ci;
	
	private JComboBox <Team>	teamBox;
	
	public void setMediator(TeamInformation ti, ConferenceInformation confInfo, CoachInformation ci)
	{
		this.ti = ti;
		this.confInfo = confInfo;
		this.ci = ci;
	}
	/**
	 * 
	 * Builds the team selection information panel.        
	 *
	 * <hr>
	 * Date created: Nov 19, 2020 
	 *
	 * 
	 * 
	 */
	public TeamInfoPanel(TeamInformation ti, ConferenceInformation confInfo, CoachInformation ci)
	{
		super();
		
		this.ti = ti;
		this.confInfo = confInfo;
		this.ci = ci;
	
		//this.conferenceList = conferenceList;
		setLayout (new BorderLayout ());
		setBorder (BorderFactory.createEtchedBorder ( ));	
		
		buildSelectionBox();
		selectedTeam = (Team)teamBox.getSelectedItem ( );
		setBackground(selectedTeam.getPrimaryColor ( ));
		
		buildInfoPanel();
		buildSchoolPanel();
		buildCoachPanel();		
		
		add(teamInfo,BorderLayout.NORTH);
		add(coachInfo,BorderLayout.WEST);
		add(schoolInfo,BorderLayout.SOUTH);
		add(teamBox,BorderLayout.EAST);
		
		teamBox.setVisible (true);
	}
	
	/**
	 * 
	 * Builds the selection box used to select a team.         
	 *
	 * <hr>
	 * Date created: Nov 19, 2020
	 *
	 * <hr>
	 */
	@SuppressWarnings ({ "unchecked", "rawtypes" })
	public void buildSelectionBox()
	{
		teamBox = new JComboBox (ti.getTeamList ().toArray ( ));
		teamBox.setRenderer (new TeamListCellRenderer ( ));
		
		teamBox.setLayout (new BorderLayout(10,10));
		teamBox.setBorder (BorderFactory.createEtchedBorder ( ));
		teamBox.addItemListener (new TeamBoxListener());

		teamBox.setSelectedIndex (0);		
	}
	
	/**
	 * 
	 * Builds the panel which contains team information.         
	 *
	 * <hr>
	 * Date created: Nov 19, 2020
	 *
	 * <hr>
	 */
	public void buildInfoPanel()
	{
		teamInfo = new JPanel();
		panelBorder = new EtchedBorder(selectedTeam.getPrimaryColor ( ), selectedTeam.getSecondaryColor ( ));
		infoTitle.setTitleJustification (TitledBorder.CENTER);
		infoTitle.setTitleColor (selectedTeam.getSecondaryColor ( ));
		infoTitle.setBorder (panelBorder);
		
		teamInfo.setLayout (new BorderLayout());
		teamInfo.setBorder (new CompoundBorder(panelBorder, infoTitle));
		teamInfo.setBackground (selectedTeam.getPrimaryColor ( ));
		
		teamLabel = new JLabel(selectedTeam.getName ( ) + " " + selectedTeam.getMascot ( ));
		teamRatingLabel = new JLabel(String.valueOf (selectedTeam.getRating ( )));
		conferenceLabel = new JLabel(selectedTeam.getConference ( ));	
		
		teamLabel.setHorizontalAlignment (JLabel.LEFT);
		teamRatingLabel.setHorizontalAlignment (JLabel.CENTER);
		conferenceLabel.setHorizontalAlignment (JLabel.RIGHT);
		
		teamLabel.setForeground (selectedTeam.getTertiaryColor ( ));
		teamRatingLabel.setForeground(selectedTeam.getTertiaryColor ( ));
		conferenceLabel.setForeground (selectedTeam.getTertiaryColor ( ));
		
		teamRatingLabel.setToolTipText (selectedTeam.getName ( ) + "'s rating is " + selectedTeam.getRating ( ) + ".");
		conferenceLabel.setToolTipText (selectedTeam.getName ( ) + " is a member of the " + getConferenceName(selectedTeam.getConference ( ), confInfo.getConfList ( )) + ".");
		
		teamInfo.add (teamLabel, BorderLayout.WEST);
		teamInfo.add (teamRatingLabel, BorderLayout.CENTER);
		teamInfo.add (conferenceLabel, BorderLayout.EAST);	
	}
	
	/**
	 * 
	 * Builds the panel which contains coach information.         
	 *
	 * <hr>
	 * Date created: Nov 19, 2020
	 *
	 * <hr>
	 * @param coach
	 */
	public void buildCoachPanel()
	{
		
		coachInfo = new JPanel();
		panelBorder = new EtchedBorder(selectedTeam.getPrimaryColor ( ), selectedTeam.getSecondaryColor ( ));
		coachTitle.setTitleColor (selectedTeam.getSecondaryColor ( ));
		coachTitle.setBorder (panelBorder);
		
		selectedCoach = searchHcList(ci.getHeadCoachMap ( ), selectedTeam);
		selectedOc = searchOcList(ci.getOcMap ( ), selectedTeam);
		selectedDc = searchDcList(ci.getDcMap ( ), selectedTeam);
		
		hcLabel = new JLabel("Head Coach: " + selectedCoach.getName ( ));
		ocLabel = new JLabel("Offensive Coordinator: ");
		dcLabel = new JLabel("Defensive Coordinator: ");
		
		coachInfo.setLayout (new GridLayout(3,1));
		coachInfo.setBorder (new CompoundBorder(panelBorder, coachTitle));
		coachInfo.setBackground (selectedTeam.getPrimaryColor ( ));
		
		hcLabel.setHorizontalAlignment (JLabel.LEFT);
		ocLabel.setHorizontalAlignment (JLabel.LEFT);
		dcLabel.setHorizontalAlignment (JLabel.LEFT);
		
		hcLabel.setForeground (selectedTeam.getTertiaryColor ( ));
		ocLabel.setForeground (selectedTeam.getTertiaryColor ( ));
		dcLabel.setForeground (selectedTeam.getTertiaryColor ( ));
		
		hcLabel.setToolTipText (selectedCoach.getName ( ) + " is head coach of " + selectedTeam.getName ( ));
		
		coachInfo.add (hcLabel);
		coachInfo.add (ocLabel);
		coachInfo.add (dcLabel);		
	}
	
	/**
	 * 
	 * @desc Builds the panel which contains information about the school.         
	 * 
	 * <hr>
	 * Date created: Nov 19, 2020
	 *
	 * <hr>
	 */
	public void buildSchoolPanel()
	{
		schoolInfo = new JPanel();
		panelBorder = new EtchedBorder(selectedTeam.getPrimaryColor ( ), selectedTeam.getSecondaryColor ( ));
		schoolTitle.setTitleColor (selectedTeam.getSecondaryColor ( ));
		schoolTitle.setBorder (panelBorder);
		
		rivalTeam = searchPrimaryRivalMap(ti.getPrimaryRivalryMap ( ), selectedTeam);
		
		schoolInfo.setLayout(new GridLayout(3,1));
		schoolInfo.setBackground (selectedTeam.getPrimaryColor ( ));
		
		stadiumNameLabel = new JLabel("Stadium: " + selectedTeam.getStadium ( ));
		locLabel = new JLabel("City: " + selectedTeam.getCity ( ) + ", " + selectedTeam.getState ( ));
		rivalLabel = new JLabel("Rival: " + rivalTeam.getName ( ));
		
		stadiumNameLabel.setHorizontalAlignment (JLabel.CENTER);
		locLabel.setHorizontalAlignment (JLabel.CENTER);
		rivalLabel.setHorizontalAlignment (JLabel.CENTER);
		
		stadiumNameLabel.setToolTipText (selectedTeam.getName ( ) + " plays home games at " + selectedTeam.getStadium ( ) + ".");
		locLabel.setToolTipText (selectedTeam.getName ( ) + " is located in " + selectedTeam.getCity ( ) + ", " + selectedTeam.getState ( ) + ".");
		rivalLabel.setToolTipText (selectedTeam.getName ( ) + "'s primary rivals are null.");
		
		stadiumNameLabel.setForeground (selectedTeam.getTertiaryColor ( ));
		locLabel.setForeground (selectedTeam.getTertiaryColor ( ));
		rivalLabel.setForeground (rivalTeam.getSecondaryColor ( ));
		
		schoolInfo.setBorder (new CompoundBorder(panelBorder, schoolTitle));
		
		schoolInfo.add (stadiumNameLabel);
		schoolInfo.add (locLabel);
		schoolInfo.add (rivalLabel);
	}
	
	/**
	 * 
	 * @desc Gets conference name from tricode. Used in order to display full name in panel tooltip.         
	 *
	 * <hr>
	 * Date created: Nov 19, 2020
	 *
	 * <hr>
	 * @param triCode
	 * @param conferenceList
	 * @return
	 */
	public String getConferenceName(String triCode, ArrayList<Conference> conferenceList)
	{
		String conferenceName = new String();
		for(int i = 0; i < conferenceList.size ( ); i++)
		{
			if(triCode.equals (conferenceList.get (i).getTriCode ( )))
			{
				conferenceName = conferenceList.get (i).getName ( );
			}
		}
		return conferenceName;
	}
	
	public HeadCoach searchHcList(HashMap<Team, HeadCoach> hcMap, Team selectedTeam)
	{
		HeadCoach selectedCoach = new HeadCoach();
		for(Team key : hcMap.keySet ( ))
		{
			if(selectedTeam.getTriCode ( ).equals (key.getTriCode ( )))
			{
				selectedCoach = hcMap.get (key);
			}
		}
		return selectedCoach;
	}
	
	public Coach searchOcList(HashMap<Team, OffCoordinator> hcMap, Team selectedTeam)
	{
		Coach selectedCoach = new Coach();
		for(Team key : hcMap.keySet ( ))
		{
			if(selectedTeam.getTriCode ( ).equals (key.getTriCode ( )))
			{
				selectedCoach = hcMap.get (key);
			}
		}
		return selectedCoach;		
	}
	
	public Coach searchDcList(HashMap<Team, DefCoordinator> hcMap, Team selectedTeam)
	{
		Coach selectedCoach = new Coach();
		for(Team key : hcMap.keySet ( ))
		{
			if(selectedTeam.getTriCode ( ).equals (key.getTriCode ( )))
			{
				selectedCoach = hcMap.get (key);
			}
		}
		return selectedCoach;		
	}
	
	public Team searchPrimaryRivalMap(HashMap<Team, Team> primaryRivalMap, Team selectedTeam)
	{
		Team rival = new Team();
		for(Team team : primaryRivalMap.keySet ( ))
		{
			if(selectedTeam.equals (team))
			{
				rival = primaryRivalMap.get (team);
			}
		}
		
		return rival;
	}
	
	private class TeamBoxListener implements ItemListener
	{
		/**
		 * 
		 * When a new team is selected in the combobox, change the labels to reflect that.         
		 *
		 * <hr>
		 * Date created: Nov 19, 2020 
		 *
		 * <hr>
		 * @param e
		 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
		 */
		public void itemStateChanged(ItemEvent e)
		{
			setBackground(selectedTeam.getPrimaryColor ( ));
			selectedTeam = (Team)teamBox.getSelectedItem ( );
			selectedCoach = searchHcList(ci.getHeadCoachMap ( ), selectedTeam);
			selectedOc = searchOcList(ci.getOcMap ( ), selectedTeam);
			selectedDc = searchDcList(ci.getDcMap ( ), selectedTeam);
			rivalTeam = searchPrimaryRivalMap(ti.getPrimaryRivalryMap ( ), selectedTeam);
			
			panelBorder = new EtchedBorder(selectedTeam.getPrimaryColor ( ), selectedTeam.getSecondaryColor ( ));
			infoTitle.setTitleColor (selectedTeam.getSecondaryColor ( ));
			infoTitle.setBorder (panelBorder);
			teamInfo.setBackground (selectedTeam.getPrimaryColor ( ));
			teamInfo.setBorder (new CompoundBorder(panelBorder, infoTitle));
			
			teamLabel.setText (selectedTeam.getName ( ) + " " + selectedTeam.getMascot ( ));
			teamRatingLabel.setText (String.valueOf (selectedTeam.getRating ( )));
			conferenceLabel.setText (selectedTeam.getConference ( ));
			conferenceLabel.setToolTipText (selectedTeam.getName ( ) + " is a member of the " + getConferenceName(selectedTeam.getConference ( ), confInfo.getConfList ( )) + ".");			
			teamRatingLabel.setToolTipText (selectedTeam.getName ( ) + "'s rating is " + selectedTeam.getRating ( ) + ".");
			teamLabel.setForeground (selectedTeam.getTertiaryColor ( ));
			teamRatingLabel.setForeground(selectedTeam.getTertiaryColor ( ));
			conferenceLabel.setForeground (selectedTeam.getTertiaryColor ( ));			
			
			coachTitle.setTitleColor (selectedTeam.getSecondaryColor ( ));
			coachTitle.setBorder (panelBorder);
			
			coachInfo.setBackground (selectedTeam.getPrimaryColor ( ));
			coachInfo.setBorder (new CompoundBorder(panelBorder, coachTitle));
			hcLabel.setText ("Head Coach: " + selectedCoach.getName ( ) + " (" + selectedCoach.getRating ( ) + ", " + selectedCoach.getHeadCoachTrait ( ).toString ( ) + ")" );
			ocLabel.setText ("Offensive Coordinator: " + " (" + ") ");
			dcLabel.setText ("Defensive Coordinator: " + ") ");
			hcLabel.setToolTipText (selectedCoach.getName ( ) + " is head coach of " + selectedTeam.getName ( ));
			
			hcLabel.setForeground (selectedTeam.getTertiaryColor ( ));
			ocLabel.setForeground (selectedTeam.getTertiaryColor ( ));
			dcLabel.setForeground (selectedTeam.getTertiaryColor ( ));
			
			schoolTitle.setTitleColor (selectedTeam.getSecondaryColor ( ));
			schoolTitle.setBorder (panelBorder);
			
			schoolInfo.setBackground (selectedTeam.getPrimaryColor ( ));
			schoolInfo.setBorder (new CompoundBorder(panelBorder, schoolTitle));
			
			stadiumNameLabel.setText (selectedTeam.getStadium ( ));
			stadiumNameLabel.setToolTipText (selectedTeam.getName ( ) + " plays home games at " + selectedTeam.getStadium ( ) + ".");			
			locLabel.setText (selectedTeam.getCity ( ) + ", " + selectedTeam.getState());
			locLabel.setToolTipText (selectedTeam.getName ( ) + " is located in " + selectedTeam.getCity ( ) + ", " + selectedTeam.getState ( ) + ".");
			rivalLabel.setText ("Rival: " + rivalTeam.getName ( ) + " " + rivalTeam.getMascot ( ));
			rivalLabel.setToolTipText (selectedTeam.getName ( ) + "'s primary rival is " + rivalTeam.getName ( ) + ".");
			conferenceLabel.setToolTipText (selectedTeam.getName ( ) + " is a member of the " + getConferenceName(selectedTeam.getConference ( ), confInfo.getConfList ( )) + ".");		
			
			stadiumNameLabel.setForeground (selectedTeam.getTertiaryColor ( ));
			locLabel.setForeground (selectedTeam.getTertiaryColor ( ));
			rivalLabel.setForeground (rivalTeam.getSecondaryColor ( ));
			
		
			repaint();
			revalidate();
		}
	}
	
}
