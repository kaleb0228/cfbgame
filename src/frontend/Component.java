/**
 * ---------------------------------------------------------------------------
 * File name: Component.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 23, 2020
 * ---------------------------------------------------------------------------
 */

package frontend;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 23, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public interface Component
{
	public void setMediator(MenuMediator mediator);
	//public String getName();
}
