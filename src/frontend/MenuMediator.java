/**
 * ---------------------------------------------------------------------------
 * File name: Mediator.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 23, 2020
 * ---------------------------------------------------------------------------
 */

package frontend;

import java.util.ArrayList;
import java.util.HashMap;
import cfbgame.Conference;
import cfbgame.HeadCoach;
import cfbgame.Team;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 23, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public interface MenuMediator
{
	public void setTeamList(ArrayList<Team> teamList);
	public void setConfList(ArrayList<Conference> confList);
	public void setCoachList(ArrayList<HeadCoach> coachList);
	public ArrayList<Conference> getConfList();
	public ArrayList<Team> getTeamList();
	public ArrayList<HeadCoach> getCoachList();
	public HashMap<Team, HeadCoach> getHcAssignment();
}
