/**
 * ---------------------------------------------------------------------------
 * File name: GameInterface.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course: CSCI 1260
 * Creation Date: Nov 17, 2020
 * ---------------------------------------------------------------------------
 */

package frontend;

import java.awt.BorderLayout;
import javax.swing.JFrame;

/**
 * Game welcome screen.
 *
 * <hr>
 * Date created: Nov 17, 2020
 * <hr>
 * 
 * @author Kaleb Dippold
 */
public class GameInterface extends JFrame
{
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 * Builds an interface using BorderLayout from the various component classes.
	 *
	 * <hr>
	 * Date created: Nov 18, 2020 
	 *
	 *
	 */
	public GameInterface (TeamInfoPanel ip)
	{
		super ();
		setSize(640,480);
		setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		setLayout (new BorderLayout ( ));
		
		add (ip);
		
		//pack ( );
		setLocationRelativeTo (null);
		setVisible (true);
	}
}
