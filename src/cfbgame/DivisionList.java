/**
 * ---------------------------------------------------------------------------
 * File name: DivisionList.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Holds a list of all divisions
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public final class DivisionList
{
	private ArrayList<Division> divisionList = new ArrayList<>();
	
	public ArrayList<Division> getDivisionList()
	{
		return divisionList;
	}
	
	public void setDivisionList(ArrayList<Division> divisionList)
	{
		this.divisionList = divisionList;
	}
	
	public Division findDivision(String triCode)
	{
		Division division = new Division();
		for(int i = 0; i < divisionList.size ( ); i++)
		{
			if(triCode.equals(divisionList.get (i).getTriCode ( )))
			{
				division = divisionList.get (i);
			}
		}
		return division;
	}		
}
