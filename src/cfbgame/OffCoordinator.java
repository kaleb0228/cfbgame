/**
 * ---------------------------------------------------------------------------
 * File name: OffensiveCoordinator.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 19, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Offensive coordinator - in reality, responsibilities vary from coaching staff to coaching staff, but generally are in charge of
 * the offense and playcalling on offense.
 *
 * <hr>
 * Date created: Nov 19, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class OffCoordinator extends Coach
{
	private CoachTrait.OffCoordinatorTrait ocTrait; 
	
	public OffCoordinator()
	{
		super("No Name", 50, 50, CoachTrait.OFFENSIVE_BACKGROUND);
		setOcTrait (CoachTrait.OffCoordinatorTrait.POSSESSION_FOCUS);
	}
	
	public OffCoordinator(String name, int rating, int age, CoachTrait coachTrait, CoachTrait.OffCoordinatorTrait ocTrait)
	{
		super(name, rating, age, coachTrait);
		this.setOcTrait (ocTrait);
	}

	
	/**
	 * @return ocTrait
	 */
	public CoachTrait.OffCoordinatorTrait getOcTrait ( )
	{
		return ocTrait;
	}

	
	/**
	 * @param ocTrait the ocTrait to set
	 */
	public void setOcTrait (CoachTrait.OffCoordinatorTrait ocTrait)
	{
		this.ocTrait = ocTrait;
	}
}
