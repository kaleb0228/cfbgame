/**
 * ---------------------------------------------------------------------------
 * File name: DefinitionsInitializer.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 24, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 24, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class DefinitionsInitializer
{
	String userDirectory = System.getProperty ("user.dir");
	
	private TeamLoader teamLoader;
	private ConferenceLoader confLoader;
	private CoachLoader coachLoader;
	private DivisionLoader divisionLoader;
	private TeamInitializer teamInitializer;
	private ConferenceInitializer confInitializer;
	private CoachInitializer coachInitializer;
	private DivisionInitializer divisionInitializer;
	private ConferenceDivisionAssignment confDivAssignment;
	private TeamScheduleInitializer teamScheduleInitializer;
	
	private TeamList teamList = new TeamList();
	private RivalryList rivalryList = new RivalryList();
	private ConferenceList confList = new ConferenceList();
	private DivisionList divList = new DivisionList();
	private HeadCoachList headCoachList = new HeadCoachList();
	private OffCoordinatorList offCoordinatorList = new OffCoordinatorList();
	private DefCoordinatorList defCoordinatorList = new DefCoordinatorList();
	private CoachAssignment coachAssignment;
	
	public DefinitionsInitializer(TeamLoader teamLoader, ConferenceLoader confLoader, CoachLoader coachLoader, DivisionLoader divLoader)
	{
		this.teamLoader = teamLoader;
		this.confLoader = confLoader;
		this.coachLoader = coachLoader;
		this.divisionLoader = divLoader;
	}
	
	public void initializeDefinitions()
	{
		initializeTeams();
		initializeConferences();
		initializeCoaches();
		initializeDivisions();
		assignConfDivisions();
		initializeSchedules();
	}
	
	private void initializeTeams()
	{
		teamInitializer = new TeamInitializer(teamLoader, teamList, rivalryList);
		teamInitializer.buildTeams ( );		
		teamList = teamInitializer.getTeamList ( );
	}
	
	private void initializeConferences()
	{
		confInitializer = new ConferenceInitializer(confLoader, confList, teamList);
		confInitializer.buildConferences ( );
		confList = confInitializer.getConferenceList ( );
	}
	
	private void initializeCoaches()
	{
		coachInitializer = new CoachInitializer(coachLoader, teamList);
		coachInitializer.buildCoaches ( );
		headCoachList.setHcList (coachInitializer.getHcList ( ));
		offCoordinatorList.setOcList (coachInitializer.getOcList ( ));
		defCoordinatorList.setDcList (coachInitializer.getDcList ( ));
		headCoachList.setHcAssignment (coachInitializer.getHcMap ( ));
		coachAssignment = new CoachAssignment(coachInitializer.getHcMap ( ), coachInitializer.getOcMap ( ), coachInitializer.getDcMap ( ));
	}
	
	private void initializeDivisions()
	{
		divisionInitializer = new DivisionInitializer(divisionLoader, divList, teamList);
		divisionInitializer.buildDivisions ( );
		divList = divisionInitializer.getDivisionList ( );
	}
	
	private void assignConfDivisions()
	{
		confDivAssignment = new ConferenceDivisionAssignment(confList, divList);
		confDivAssignment.assignDivisions ( );
	}
	
	private void initializeSchedules()
	{
		teamScheduleInitializer = new TeamScheduleInitializer(divList, teamList, rivalryList, confList);
		teamScheduleInitializer.initializeTeamSchedules ( );
		teamScheduleInitializer.populateTeamOpponents ( );
		teamScheduleInitializer.createMasterSchedule ( );
	}
	
	public DivisionList getDivisionList()
	{
		return divList;
	}
	
	public TeamList getTeamList()
	{
		return teamList;
	}
	
	public RivalryList getRivalryList()
	{
		return rivalryList;
	}	
	
	public ConferenceList getConfList()
	{
		return confList;
	}
	
	public HeadCoachList getHeadCoachList()
	{
		return headCoachList;
	}
	
	public OffCoordinatorList getOffCoordinatorList()
	{
		return offCoordinatorList;
	}
	
	public DefCoordinatorList getDefCoordinatorList()
	{
		return defCoordinatorList;
	}
	
	public CoachAssignment getCoachAssignment()
	{
		return coachAssignment;
	}
}
