/**
 * ---------------------------------------------------------------------------
 * File name: HeadCoach.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 19, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Head coach of a program.
 *
 * <hr>
 * Date created: Nov 19, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class HeadCoach extends Coach
{
	private CoachTrait.HeadCoachTrait headCoachTrait;
	
	public HeadCoach()
	{
		super("No Name", 50, 50, CoachTrait.OFFENSIVE_BACKGROUND);
		setHeadCoachTrait (CoachTrait.HeadCoachTrait.CEO);
		
	}
	
	public HeadCoach(String name, int rating, int age, CoachTrait coachTrait, CoachTrait.HeadCoachTrait hcTrait)
	{
		super(name, rating, age, coachTrait);
		this.setHeadCoachTrait (hcTrait);
	}

	
	/**
	 * @return headCoachTrait
	 */
	public CoachTrait.HeadCoachTrait getHeadCoachTrait ( )
	{
		return headCoachTrait;
	}

	
	/**
	 * @param headCoachTrait the headCoachTrait to set
	 */
	public void setHeadCoachTrait (CoachTrait.HeadCoachTrait headCoachTrait)
	{
		this.headCoachTrait = headCoachTrait;
	}	
}
