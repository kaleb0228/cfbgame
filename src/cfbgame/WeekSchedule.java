/**
 * ---------------------------------------------------------------------------
 * File name: WeekSchedule.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Dec 3, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Weekly schedules
 *
 * <hr>
 * Date created: Dec 3, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class WeekSchedule
{
	private ArrayList<Match> weekSchedule = new ArrayList<>();
	ArrayList<Team> unavailableTeams = new ArrayList<>();
	private MatchBuilder matchBuilder;
	private TeamOpponents opponents;
	private TeamList teamList;
	private RivalryList rivalryList;	
	private ArrayList<Team> oppositeDivision;
	private ArrayList<Team> division;
	private ArrayList<Team> availableTeams;
	private ArrayList<ArrayList<Match>> masterSchedule;
	private ListSearch ls;
	private WeekScheduleDebugFile ws;
	
	public WeekSchedule(MatchBuilder mb, TeamList teamList, RivalryList rivalryList)
	{
		this.matchBuilder = mb;
		this.teamList = teamList;
		this.rivalryList = rivalryList;
		availableTeams = new ArrayList<Team>(teamList.getTeamList ( ));
		ls = new ListSearch();
	}
	
	public void setTeam(TeamOpponents opponents)
	{
		this.opponents = opponents;
	}
	
	/**
	 * 
	 * Creates the matchups for every week. Week zero reserved for special cases, weeks 1-4 will generally have non-conference matchups, and weeks 5-13 will generally have
	 * conference matchups.         
	 *
	 * <hr>
	 * Date created: Dec 4, 2020
	 *
	 * <hr>
	 * @param week
	 */
	public ArrayList<Match> fillWeekSchedule(int week)
	{
		ArrayList<Match> prelimWeekSchedule = new ArrayList<>();
		Collections.shuffle (availableTeams); // Shuffle the list, will always start with a random team to ensure fairness.
		for(int i = 0; i < availableTeams.size ( ); i++)
		{
			Team team1 = availableTeams.get (i);
			if(unavailableTeams.contains (team1)) // If team has already been scheduled a match, skip this one and advance to next.
			{
				continue;
			}
			System.out.println("Setting week " + week + " opponent for team " + team1.getName ( ));
			if(week >= 0 && week < 4)
			{
				Random ran = new Random();
				double oocOrConference = ran.nextDouble ( );
				if(oocOrConference <= 0.10 && !unavailableTeams.containsAll (ls.getCrossDivision (availableTeams, team1)))
				{
					Team team2 = null;
					do
					{
						team2 = ls.getRandomCrossDivisionOpponent (availableTeams, team1);
					} while(validMatchup(team1, team2, week) == false);
					prelimWeekSchedule.add (matchBuilder.buildMatch (team1, team2));
					unavailableTeams.add (team1);
					if(!team2.getTriCode ( ).equals ("BYE"))
					{
						unavailableTeams.add (team2);						
					}
					System.out.println("Teams " + team1.getName ( ) + " and " + team2.getName ( ) + " will play in week " + week + "." );				
				}
				else if(oocOrConference <= 0.20 && !unavailableTeams.containsAll (ls.getDivision (availableTeams, team1)))
				{
					Team team2 = null;
					do
					{
						team2 = ls.getRandomDivisionOpponent (availableTeams, team1);
					} while(validMatchup(team1, team2, week) == false);
					prelimWeekSchedule.add (matchBuilder.buildMatch (team1, team2));	
					unavailableTeams.add (team1);
					if(!team2.getTriCode ( ).equals ("BYE"))
					{
						unavailableTeams.add (team2);						
					}
					System.out.println("Teams " + team1.getName ( ) + " and " + team2.getName ( ) + " will play in week " + week + "." );					
				}
				else if(oocOrConference <= 1.0)
				{
					Team team2 = null;
					do
					{
						team2 = ls.getRandomOutOfConferenceOpponent (availableTeams, team1);
					} while (validMatchup(team1, team2, week) == false);
					prelimWeekSchedule.add (matchBuilder.buildMatch (team1, team2));
					unavailableTeams.add (team1);
					if(!team2.getTriCode ( ).equals ("BYE"))
					{
						unavailableTeams.add (team2);						
					}
					System.out.println("Teams " + team1.getName ( ) + " and " + team2.getName ( ) + " will play in week " + week + "." );					
				}
			}
			else if(week > 4 && week < 15)
			{
				Random ran = new Random();
				double oocOrConference = ran.nextDouble ( );
				if(oocOrConference <= 0.20)
				{
					Team team2 = null;
					do
					{
						team2 = ls.getRandomOutOfConferenceOpponent (availableTeams, team1);
					} while (validMatchup(team1, team2, week) == false);
					prelimWeekSchedule.add (matchBuilder.buildMatch (team1, team2));
					unavailableTeams.add (team1);
					if(!team2.getTriCode ( ).equals ("BYE"))
					{
						unavailableTeams.add (team2);						
					}
					System.out.println("Teams " + team1.getName ( ) + " and " + team2.getName ( ) + " will play in week " + week + "." );
				}
				else if (oocOrConference <= 0.50)
				{
					Team team2 = null;
					do
					{
						team2 = ls.getRandomCrossDivisionOpponent (availableTeams, team1);
					} while(validMatchup(team1, team2, week) == false);
					prelimWeekSchedule.add (matchBuilder.buildMatch (team1, team2));
					unavailableTeams.add (team1);
					if(!team2.getTriCode ( ).equals ("BYE"))
					{
						unavailableTeams.add (team2);						
					}
					System.out.println("Teams " + team1.getName ( ) + " and " + team2.getName ( ) + " will play in week " + week + "." );
				}
				else if (oocOrConference <= 1.0)
				{
					Team team2 = null;
					do
					{
						team2 = ls.getRandomDivisionOpponent (availableTeams, team1);
					} while(validMatchup(team1, team2, week) == false);
					prelimWeekSchedule.add (matchBuilder.buildMatch (team1, team2));	
					unavailableTeams.add (team1);
					if(!team2.getTriCode ( ).equals ("BYE"))
					{
						unavailableTeams.add (team2);						
					}
					System.out.println("Teams " + team1.getName ( ) + " and " + team2.getName ( ) + " will play in week " + week + "." );
				}
			}
			else if(week == team1.getRivalryWeek ( ) && opponents.getOocRival (rivalryList) != null)
			{
				prelimWeekSchedule.add (matchBuilder.buildMatch (opponents.getTeam ( ), opponents.getOocRival (rivalryList)));
			}
		}
		availableTeams.removeAll (unavailableTeams);
		ws = new WeekScheduleDebugFile(prelimWeekSchedule);
		ws.printDebugFile ( );
		return prelimWeekSchedule;
	}
	
	public boolean validMatchup(Team team1, Team team2, int week)
	{
		boolean validMatchup = false;
		if(unavailableTeams.size ( ) == 0)
		{
			validMatchup = true;
		}
		else
		{
			Collections.sort (unavailableTeams);
			for(int i = 0; i < unavailableTeams.size ( ); i++)
			{
				if(unavailableTeams.get (i).getTriCode ( ).equals (team1.getTriCode ( )) || unavailableTeams.get (i).getTriCode ( )
								.equals (team2.getTriCode ( )))
				{
					validMatchup = false;
					break;
				}
				else
				{
					validMatchup = true;
				}				
			}			
		}
		return validMatchup;
	}
	
	public Team getRandomCrossDivisionTeam()
	{
		Team randomCrossDivisionTeam = opponents.getRandomCrossDivisionTeam (oppositeDivision);
		oppositeDivision.remove (randomCrossDivisionTeam);
		availableTeams.remove (randomCrossDivisionTeam);
		return randomCrossDivisionTeam;
	}
	
	public Team getRandomDivisionTeam()
	{
		Team randomDivisionTeam = opponents.getRandomDivisionTeam (division);
		division.remove (randomDivisionTeam);
		availableTeams.remove (randomDivisionTeam);
		return randomDivisionTeam;
	}
	
	public Team getRandomOutOfConferenceTeam()
	{
		Team randomOutOfConferenceTeam = opponents.getRandomOutOfConferenceTeam (availableTeams);
		availableTeams.remove (randomOutOfConferenceTeam);
		return randomOutOfConferenceTeam;
	}
	
	private class ListSearch
	{
		
		public List<Team> getCrossDivision(List<Team> opponentList, Team team)
		{
			List <Team> crossDivisionOpponents = opponentList.stream ( )
							.filter (t -> t.getConference ( ).equals (team.getConference ( )))
							.filter (t -> !t.getDivision ( ).equals (team.getDivision ( )))
							.filter (t -> !t.getTriCode ( ).equals ("BYE"))
							.collect (Collectors.toList ( ));
			return crossDivisionOpponents;
		}
		
		public List<Team> getDivision(List<Team> opponentList, Team team)
		{
			List <Team> divisionOpponents = opponentList.stream ( )
							.filter (t -> t.getDivision ( ).equals (team.getDivision ( )))
							.filter (t -> !t.getTriCode ( ).equals ("BYE"))
							.collect (Collectors.toList ( ));
			return divisionOpponents;
		}

		public Team getRandomOutOfConferenceOpponent(List<Team> opponentList, Team team)
		{
			List<Team> outOfConferenceTeams = opponentList.stream ( ).filter (t -> !t.getConference ( ).equals (team.getConference ( )))
							.collect (Collectors.toList ( ));
			Collections.shuffle (outOfConferenceTeams);
			return outOfConferenceTeams.get (0);			
		}
		
		public Team getRandomDivisionOpponent(List<Team> opponentList, Team team)
		{
			List<Team> divisionOpponents = opponentList.stream ( )
							.filter (t -> !t.getTriCode ( ).equals (team.getTriCode ( )))
							//.filter (t -> t.getDivision ( ).equals (team.getDivision ( )))
							.collect (Collectors.toList ( ));
			Collections.shuffle (divisionOpponents);
			
			return divisionOpponents.get (0);
		}
		
		public Team getRandomCrossDivisionOpponent(List<Team> opponentList, Team team)
		{
			List <Team> crossDivisionOpponents = opponentList.stream ( )
							.filter (t -> t.getConference ( ).equals (team.getConference ( )))
							.filter (t -> !t.getDivision ( ).equals (team.getDivision ( )))
							.filter (t -> !t.getTriCode ( ).equals ("BYE"))
							.collect (Collectors.toList ( ));
			Collections.shuffle (crossDivisionOpponents);	
			
			return crossDivisionOpponents.get (0);
		}
		
		public int findTeamIndex(ArrayList<Team> teamList, String triCode)
		{
			int index = 0;
			for(int i = 0; i < teamList.size ( ); i++)
			{
				if(teamList.get (i).getTriCode ( ).equals (triCode));
				{
					index = i;					
				}
			}
			return index;
		}
		
		public boolean searchWeekMatchups(ArrayList<Match> matchList, Team team1, Team team2)
		{
			boolean isScheduled = false;
			for(int i = 0; i < matchList.size ( ); i++)
			{
				if(matchList.get (i).getMatchup (team1, team2) == false)
				{
					isScheduled = false;
				}
				else if (matchList.get (i).getMatchup (team1, team2) == true)
				{
					isScheduled = true;
				}
			}
			return isScheduled;
		}
	}
	
	private class WeekScheduleDebugFile
	{
		String userDirectory = System.getProperty ("user.dir");
		File masterScheduleChart = new File(userDirectory + "//testing//scheduleOutput//masterSchedule" + ".csv");
		ArrayList<Match> weekSchedule;
		FileWriter fw;
		
		public WeekScheduleDebugFile(ArrayList<Match> weekSchedule)
		{
			this.weekSchedule = weekSchedule;
			try
			{
				fw = new FileWriter(masterScheduleChart);
			}
			catch(IOException e)
			{
				System.out.println(e.getMessage ( ));
			}
		}
		
		public void printDebugFile()
		{
			try
			{
				fw.append ("AWAY TEAM");
				fw.append (",");
				fw.append ("HOME TEAM");
				fw.append (System.getProperty ("line.separator"));
				for(int i = 0; i < weekSchedule.size ( ); i++)
				{
					fw.append (weekSchedule.get (i).getTeam1 ( ).getName ( ) + "," + weekSchedule.get (i).getTeam2 ( ).getName ( ));
					fw.append (System.getProperty ("line.separator"));
				}
				fw.close ( );
			}
			catch(IOException e)
			{
				System.out.println(e.getMessage ( ));
			}
		}
	}	
}
