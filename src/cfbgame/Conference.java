/**
 * ---------------------------------------------------------------------------
 * File name: Conference.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 17, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Schools are usually members of an athletic conference.
 *
 * <hr>
 * Date created: Nov 17, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class Conference extends Organization
{
	private ArrayList<Team> confMembers = new ArrayList<>();
	private ArrayList<Division> confDivisions = new ArrayList<>();
	private boolean independentRules;

	public Conference()
	{
		super();
	}
	
	public Conference (String name, String triCode, int rating) throws Exception
	{
		super(name, triCode, rating);
		independentRules = false;
	}
	
	public void addMember(Team team)
	{
		confMembers.add (team);
		team.setConference (getName ( ));
	}

	public void addDivision(Division division)
	{
		confDivisions.add (division);
	}
	
	public ArrayList<Division> getDivisions()
	{
		return confDivisions;
	}
	
	public Division findDivision(String triCode)
	{
		Division division = new Division();
		for(int i = 0; i < confDivisions.size ( ); i++)
		{
			if(confDivisions.get (i).getTriCode ( ).equals (triCode))
			{
				division = confDivisions.get (i);
			}
		}
		return division;
	}
	
	public ArrayList<Team> getConferenceMembers()
	{
		return confMembers;
	}
	
	public Conference findConference(String triCode)
	{
		if(triCode.equals (this.triCode));
		return this;
	}	
	
	@Override
	public String toString()
	{
		return super.toString ( ) + "\nMembers:\n" + confMembers.toString ( );
	}
}
