/**
 * ---------------------------------------------------------------------------
 * File name: CoachTraits.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 20, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 20, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public enum CoachTrait
{
	OFFENSIVE_BACKGROUND, DEFENSIVE_BACKGROUND, SPECIAL_TEAMS_BACKGROUND, PLAYER_BACKGROUND;
	
	@Override
	public String toString()
	{
		switch(this)
		{
			case OFFENSIVE_BACKGROUND:     return "Offensive Background";
			case DEFENSIVE_BACKGROUND:     return "Defensive Background";
			case SPECIAL_TEAMS_BACKGROUND: return "Special Teams Background";
			case PLAYER_BACKGROUND:        return "Playing Background";
			default:					   throw new IllegalArgumentException();
		}
	}
	
	public enum HeadCoachTrait
	{
		MASTER_COACH, CEO, PLAYERS_COACH, FOOTBALL_GENIUS, RECRUITER, SALESMAN, OFFENSIVE_MINDED, DEFENSIVE_MINDED;
		
		@Override
		public String toString()
		{
			switch(this)
			{
				case MASTER_COACH:     return "Program Legend";
				case CEO:              return "CEO";
				case PLAYERS_COACH:    return "Player's Coach";
				case FOOTBALL_GENIUS:  return "X's and O's Coach";
				case RECRUITER:        return "Recruiter";
				case SALESMAN:         return "Salesman";
				case OFFENSIVE_MINDED: return "Offensive Background";
				case DEFENSIVE_MINDED: return "Defensive Background";
				default:               throw new IllegalArgumentException();
			}
		}		
	}
	
	
	public enum OffCoordinatorTrait
	{
		MASTERMIND_OFF, PASSING_FOCUS, RUSHING_FOCUS_OFF, TRICKSTER_OFF, SPEED_FOCUS_OFF, POSSESSION_FOCUS;
		
		@Override
		public String toString()
		{
			switch(this)
			{
				case MASTERMIND_OFF:     return "Offensive Mastermind";
				case PASSING_FOCUS:      return "Passing Game Focus";
				case RUSHING_FOCUS_OFF:  return "Rushing Game Focus";
				case TRICKSTER_OFF:      return "Offensive Trickster";
				case SPEED_FOCUS_OFF:    return "Aggressive Offense";
				case POSSESSION_FOCUS:   return "Conservative Offense";
				default:                 throw new IllegalArgumentException();
			}
		}		
	}
	
	public enum DefCoordinatorTrait
	{
		MASTERMIND_DEF, COVER_FOCUS, RUSHING_FOCUS_DEF, TRICKSTER_DEF, SPEED_FOCUS_DEF, TURNOVER_FOCUS;
		
		@Override
		public String toString()
		{
			switch(this)
			{
				case MASTERMIND_DEF:    return "Defensive Mastermind";
				case COVER_FOCUS:       return "Coverage Game Focus";
				case RUSHING_FOCUS_DEF: return "Run Stopping Focus";
				case TRICKSTER_DEF:     return "Defensive Trickster";
				case SPEED_FOCUS_DEF:   return "Athletic Defense";
				case TURNOVER_FOCUS:    return "Aggressive Defense";
				default:                throw new IllegalArgumentException();
			}
		}
	}
}
