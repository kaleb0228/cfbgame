/**
 * ---------------------------------------------------------------------------
 * File name: TeamBuildere.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class TeamInitializer
{
	private String userDirectory = System.getProperty ("user.dir");
	private TeamList teamList = new TeamList();
	private RivalryList rivalryList = new RivalryList();
	private TeamLoader teamLoader;
	
	public TeamInitializer(TeamLoader teamLoader, TeamList teamList, RivalryList rivalryList)
	{
		this.teamLoader = teamLoader;
		this.teamList = teamList;
		this.rivalryList = rivalryList;
	}
	
	public void setTeamLoader(TeamLoader teamLoader)
	{
		this.teamLoader = teamLoader;
	}
	
	public void setTeamList(TeamList teamList)
	{
		this.teamList = teamList;
	}
	
	public TeamList getTeamList()
	{
		return teamList;
	}
	
	public void setRivalryList(RivalryList rivalryList)
	{
		this.rivalryList = rivalryList;
	}
	
	public RivalryList getRivalryList()
	{
		return rivalryList;
	}
	
	public void buildTeams()
	{
		try
		{
			teamLoader.setRivalryList (rivalryList);
			teamLoader.loadFile ( );
			teamLoader.populate ( );	
			teamLoader.loadTeamColors (userDirectory + "//definitions//teamColors.txt");
			teamLoader.loadTeamRivalries (userDirectory + "//definitions//rivalries.txt");
			teamList.setTeamList (teamLoader.getTeamList ( ));
			rivalryList.setPrimaryRivalryMap (teamLoader.getPrimaryRivalryList ( ));
			rivalryList.setCrossDivisionRivalryMap (teamLoader.getSecondaryRivalryList ( ));			
		}
		catch(FileNotFoundException e)
		{
			System.out.println(e.getMessage ( ));
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage ( ));
		}
	}
}
