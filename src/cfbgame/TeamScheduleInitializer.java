/**
 * ---------------------------------------------------------------------------
 * File name: TeamScheduleInitializer.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 27, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 27, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class TeamScheduleInitializer
{
	String userDirectory = System.getProperty ("user.dir");
	private TeamList teamList;
	private RivalryList rivalryList;
	private DivisionList divisionList;
	private ConferenceList confList;
	private ArrayList<ArrayList<Match>> masterSchedule = new ArrayList<>();
	private TeamScheduleList scheduleList = new TeamScheduleList();
	
	public TeamScheduleInitializer(DivisionList divisionList, TeamList teamList, RivalryList rivalryList, ConferenceList confList)
	{
		this.teamList = teamList;
		this.rivalryList = rivalryList;
		this.divisionList = divisionList;
		this.confList = confList;
	}
	
	public void initializeTeamSchedules()
	{
		MatchBuilder mb = new MatchBuilder();
		WeekSchedule ws = new WeekSchedule(mb, teamList, rivalryList);
		ws.fillWeekSchedule (5);
	}
	
	public void createTeamOpponents()
	{
	}
	
	public void populateTeamOpponents()
	{
	}
	
	public void createWeeklySchedules(TeamOpponents to)
	{
	}
	
	private void createTeamSchedules(TeamOpponents to)
	{
	}
	
	public void createMasterSchedule()
	{
	}
}
