/**
 * ---------------------------------------------------------------------------
 * File name: DefCoordinatorList.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class DefCoordinatorList
{
	private ArrayList<DefCoordinator> dcList = new ArrayList<>();
	
	public DefCoordinatorList()
	{
		
	}
	
	public ArrayList<DefCoordinator> getDcList()
	{
		return dcList;
	}
	
	public void setDcList(ArrayList<DefCoordinator> dcList)
	{
		this.dcList = dcList;
	}
}
