/**
 * ---------------------------------------------------------------------------
 * File name: DivisionLoader.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class DivisionLoader extends DataLoader
{
	private TeamList teamList;
	private ArrayList<String> divFile = new ArrayList<>();
	private ArrayList<Division> divList;
	private DivisionAssignmentLoader dal;
	
	public DivisionLoader(String filePath)
	{
		super(filePath);
	}
	
	/**
	 * 
	 * Uses a FileReader wrapped in a BufferedReader in order to create a String[] of every line in the input file.
	 *
	 * <hr>
	 * Date created: Nov 18, 2020
	 *
	 * <hr>
	 * 
	 * @see cfbgame.DataLoader#loadFile()
	 */
	@Override
	public void loadFile ( ) throws FileNotFoundException, IOException
	{
		BufferedReader divReader = new BufferedReader (new FileReader (filePath), 16384);
		StringBuilder divLine = new StringBuilder ( );
		String line;

		while ( (line = divReader.readLine ( )) != null)
		{
			if ( !line.startsWith ("#"))
			{
				divLine.append (line);
				divLine.append (System.lineSeparator ( ));
				divFile.add (line);
			}
		}
		divReader.close ( );
	}
	
	/**
	 * 
	 * Populates the division array from the Division[] conferenceFile.
	 *
	 * <hr>
	 * Date created: Nov 18, 2020
	 *
	 * <hr>
	 * 
	 * @see cfbgame.DataLoader#populate()
	 */
	public void populate ( )
	{
		divList = new ArrayList<Division>();
		System.out.println ("Initializing Divisions...");
		for (int i = 0; i < divFile.size ( ); i++ )
		{
			ArrayList <String> al = new ArrayList <String> (Arrays.asList (divFile.get (i).split ("\\|"))); // Convert resulting list															 // into arrayList.
			Division division = new Division ( );
			division.setTricode (al.get (0)); // Tricode
			division.setName (al.get (1)); // Name
			division.setRating (Integer.valueOf (al.get (2))); // Rating
			division.setConference (al.get (3));
			divList.add (division); // Add to conferenceList.
		}
		System.out.println("Initialized divisions successfully.");
	}
	
	public void loadDivisionAssignments(ArrayList<Team> teamList, String triCode)
	{
		dal = new DivisionAssignmentLoader(filePath, teamList);
		dal.setFilePath (triCode);
		try
		{
			dal.loadFile ( );
			dal.populate ( );
		}
		catch(FileNotFoundException e)
		{
			System.out.println (e.getMessage ( ));
		}
		catch(IOException e)
		{
			System.out.println (e.getMessage ( ));
		}
	}
	
	public ArrayList<Division> getDivisionList()
	{
		return divList;
	}
	
	public Division findDivision(String triCode)
	{
		Division division = new Division();
		for(int i = 0; i < divList.size ( ); i++)
		{
			if(triCode.equals (divList.get (i).getTriCode ( )))
			{
				division = divList.get (i);
			}
		}
		return division;
	}
	
	private class DivisionAssignmentLoader extends DataLoader
	{
		private String divisionTriCode;
		private String teamTriCode;
		private String userDirectory = System.getProperty ("user.dir");
		private String filePath;
		
		private ArrayList<Team> teamList;
		private ArrayList<String> daFile = new ArrayList<>();
		
		public DivisionAssignmentLoader(String filePath, ArrayList<Team> teamList)
		{
			super(filePath);
			this.teamList = teamList;
		}
		
		public void setFilePath(String divTriCode)
		{
			this.filePath = (userDirectory + "\\definitions\\Divisions\\" + divTriCode + ".txt");
			this.divisionTriCode = divTriCode;
		}
		
		public void loadFile() throws FileNotFoundException, IOException
		{
			BufferedReader daReader = new BufferedReader(new FileReader(filePath), 16384);
			StringBuilder daLine = new StringBuilder();
			String line;
			
			while((line = daReader.readLine ( )) != null)
			{
				if(!line.startsWith ("#"))
				{
					daLine.append (line);
					daLine.append (System.lineSeparator ( ));
					daFile.add (line);					
				}
			}
			daReader.close ( );					
		}
		
		public void populate()
		{
			for(int i = 0; i < daFile.size ( ); i++)
			{
				teamTriCode = daFile.get (i);
				findDivision(divisionTriCode).addMember (findTeam(teamTriCode));
				setTeamDivision(teamTriCode, divisionTriCode);
			}
		}
		
		public void setTeamDivision(String tc, String dtc)
		{
			for(int i = 0; i < teamList.size ( ); i++)
			{
				if(teamList.get (i).getTriCode ( ).equalsIgnoreCase (tc))
				{
					teamList.get (i).setDivision (dtc);
				}
			}			
		}
		
		public Team findTeam(String triCode)
		{
			Team teamToFind = new Team();
			for(int i = 0; i < teamList.size ( ); i++)
			{
				if(triCode.equals (teamList.get (i).getTriCode ( )))
				{
					teamToFind = teamList.get (i);
				}
			}
			return teamToFind;			
		}		
	}
}
