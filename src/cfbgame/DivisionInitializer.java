/**
 * ---------------------------------------------------------------------------
 * File name: DivisionInitializer.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class DivisionInitializer
{
	String userDirectory = System.getProperty ("user.dir");
	
	private String[] divAssignmentFiles;
	
	private DivisionLoader divisionLoader;
	private DivisionList divisionList;
	private TeamList teamList;
	
	public DivisionInitializer(DivisionLoader divLoader, DivisionList divList, TeamList teamList)
	{
		this.divisionLoader = divLoader;
		this.divisionList = divList;
		this.teamList = teamList;
	}
	
	public void setDivisionList(DivisionList divList)
	{
		this.divisionList = divList;
	}
	
	public DivisionList getDivisionList()
	{
		return divisionList;
	}
	
	public void buildDivisions()
	{
		try
		{
			divisionLoader.loadFile ( );
			divisionLoader.populate ( );
			divisionList.setDivisionList (divisionLoader.getDivisionList ( ));
			exploreAssignmentFolder(userDirectory + "//definitions//divisions//");
			buildDivAssignments(teamList, divAssignmentFiles);
		}
		catch(FileNotFoundException e)
		{
			System.out.println(e.getMessage ( ));
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage ( ));
		}
	}
	
	private void buildDivAssignments(TeamList teamList, String[] assignmentFiles)
	{
		for(int i = 0; i < assignmentFiles.length; i++)
		{
			divisionLoader.loadDivisionAssignments (teamList.getTeamList ( ), assignmentFiles[i]);
		}
	}
	
	private void exploreAssignmentFolder(String filePath)
	{
		File f = new File(filePath);
		String[] divAssignmentFiles = f.list ( );
		for(int i = 0; i < divAssignmentFiles.length; i++)
		{
			divAssignmentFiles[i] = divAssignmentFiles[i].substring (0, divAssignmentFiles[i].lastIndexOf ('.'));
		}
		setDivAssignmentFiles(divAssignmentFiles);
	}
	
	private void setDivAssignmentFiles(String[] divAssignmentFiles)
	{
		this.divAssignmentFiles = divAssignmentFiles;
	}
}
