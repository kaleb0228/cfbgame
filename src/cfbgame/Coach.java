/**
 * ---------------------------------------------------------------------------
 * File name: Coach.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 17, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Coaches don't play.
 *
 * <hr>
 * Date created: Nov 17, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class Coach
{
	private String name;
	private int rating;
	private int age;
	private CoachTrait coachTrait;
	
	/**
	 * 
	 * Default constructor.        
	 *
	 * <hr>
	 * Date created: Nov 18, 2020 
	 *
	 *
	 */
	public Coach()
	{
		name = "Unnamed infant";
		rating = 50;
		age = 50;
		setCoachTrait (CoachTrait.OFFENSIVE_BACKGROUND);
	}
	
	public Coach(String name, int rating, int age, CoachTrait coachTrait)
	{
		this.name = name;
		this.rating = rating;
		this.age = age;
		this.setCoachTrait (coachTrait);
	}

	public String getName ( )
	{
		return name;
	}

	public void setName (String name)
	{
		this.name = name;
	}

	public int getRating ( )
	{
		return rating;
	}

	public void setRating (int rating)
	{
		this.rating = rating;
	}
	
	public int getAge ( )
	{
		return age;
	}
	
	public void setAge (int age)
	{
		this.age = age;
	}

	
	/**
	 * @return coachTrait
	 */
	public CoachTrait getCoachTrait ( )
	{
		return coachTrait;
	}

	
	/**
	 * @param coachTrait the coachTrait to set
	 */
	public void setCoachTrait (CoachTrait coachTrait)
	{
		this.coachTrait = coachTrait;
	}
	
}
