/**
 * ---------------------------------------------------------------------------
 * File name: Match.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 29, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 29, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class Match
{
	private Team awayTeam;
	private Team homeTeam;
	private Team losingTeam;
	private Team winningTeam;
	private int awayScore;
	private int homeScore;
	/*
	 * private boolean divisionMatch;
	 * private boolean conferenceMatch;
	 * private boolean rivalryMatch;
	 */
	
	public Match()
	{
		awayTeam = new Team();
		homeTeam = new Team();
		awayScore = 0;
		homeScore = 0;
	}
	
	public Match(Team awayTeam, Team homeTeam)
	{
		this.awayTeam = awayTeam;
		this.homeTeam = homeTeam;
	}
	
	public void setWinner()
	{
		if(awayScore > homeScore)
		{
			winningTeam = awayTeam;
			losingTeam = homeTeam;
		}
		else if(homeScore > awayScore)
		{
			winningTeam = homeTeam;
			losingTeam = awayTeam;
		}
	}
	
	public Team getTeam1()
	{
		return homeTeam;
	}
	
	public Team getTeam2()
	{
		return awayTeam;
	}
	
	public boolean getMatchup(Team team1, Team team2)
	{
		boolean matchupExists = false;
		if(team1.getTriCode ( ).equals (homeTeam.getTriCode ( )) || team1.getTriCode ( ).equals (awayTeam.getTriCode ( ))
			|| team2.getTriCode ( ).equals (awayTeam.getTriCode ( )) || team2.getTriCode ( ).equals (homeTeam.getTriCode ( )))
			matchupExists = true;
		else
			matchupExists = false;
		return matchupExists;
	}
	
	@Override
	public String toString()
	{
		return "Away Team: " + awayTeam.getName ( ) + ", Home Team: " + homeTeam.getName ( );
	}
}
