/**
 * ---------------------------------------------------------------------------
 * File name: Organization.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 22, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 22, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public abstract class Organization implements Comparable<Organization>
{
	public String name;
	public String triCode;
	public int rating;
	
	public Organization()
	{
		name = "No Name";
		triCode = "XXX";
		rating = 50;
	}
	
	public Organization(String name, String triCode, int rating) throws Exception
	{
		if(triCode.length ( ) > 5)
		{
			throw new Exception("Tri-code must be an abbreviation no greater than 5 characters.");
		}
		this.name = name;
		this.triCode = triCode;
		this.rating = rating;
	}
	
	public String getName ( )
	{
		return name;
	}

	public void setName (String name)
	{
		this.name = name;
	}

	public String getTriCode ( )
	{
		return triCode;
	}

	public void setTricode (String triCode)
	{
		this.triCode = triCode;
	}

	public int getRating ( )
	{
		return rating;
	}

	public void setRating (int rating)
	{
		this.rating = rating;
	}	
	
	/**
	 * Enter method description here         
	 *
	 * <hr>
	 * Date created: Dec 4, 2020 
	 *
	 * <hr>
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode ( )
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ( (name == null) ? 0 : name.hashCode ( ));
		result = prime * result + rating;
		result = prime * result + ( (triCode == null) ? 0 : triCode.hashCode ( ));
		return result;
	}
	
	@Override
	public int compareTo(Organization org)
	{
		int last = this.triCode.compareTo (org.triCode);
		return last == 0 ? this.triCode.compareTo (org.triCode) : last;
	}

	/**
	 * Enter method description here         
	 *
	 * <hr>
	 * Date created: Dec 4, 2020 
	 *
	 * <hr>
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass ( ) != obj.getClass ( ))
			return false;
		Organization other = (Organization) obj;
		if (name == null)
		{
			if (other.name != null)
				return false;
		}
		else if ( !name.equals (other.name))
			return false;
		if (rating != other.rating)
			return false;
		if (triCode == null)
		{
			if (other.triCode != null)
				return false;
		}
		else if ( !triCode.equals (other.triCode))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "Name: " + name + "\nTricode" + triCode + "\nRating: " + rating;
	}
}
