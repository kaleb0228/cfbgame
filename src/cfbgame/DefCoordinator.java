/**
 * ---------------------------------------------------------------------------
 * File name: DefCoordinator.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 19, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Defensive coordinator - in reality, responsibility varies from program to program, but generally is responsible for the defensive side of the ball
 * and playcalling on defense.
 *
 * <hr>
 * Date created: Nov 19, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class DefCoordinator extends Coach
{
	private CoachTrait.DefCoordinatorTrait dcTrait;
	
	public DefCoordinator()
	{
		super("No Name", 50, 50, CoachTrait.DEFENSIVE_BACKGROUND);
		setDcTrait (CoachTrait.DefCoordinatorTrait.COVER_FOCUS);
	}
	
	public DefCoordinator(String name, int rating, int age, CoachTrait coachTrait, CoachTrait.DefCoordinatorTrait dcTrait)
	{
		super(name, rating, age, coachTrait);
		this.setDcTrait (dcTrait);
	}

	
	/**
	 * @return dcTrait
	 */
	public CoachTrait.DefCoordinatorTrait getDcTrait ( )
	{
		return dcTrait;
	}

	
	/**
	 * @param dcTrait the dcTrait to set
	 */
	public void setDcTrait (CoachTrait.DefCoordinatorTrait dcTrait)
	{
		this.dcTrait = dcTrait;
	}
}
