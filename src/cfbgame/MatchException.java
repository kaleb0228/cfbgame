/**
 * ---------------------------------------------------------------------------
 * File name: MatchException.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 30, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Exceptions to be thrown when an invalid match is declared, for whatever reason.
 *
 * <hr>
 * Date created: Nov 30, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class MatchException extends Exception
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MatchException(String errorMessage)
	{
		super(errorMessage);
	}
	
}
