/**
 * ---------------------------------------------------------------------------
 * File name: MatchBuilder.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Dec 1, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Builds the actual matches from a team's opponents list.
 *
 * <hr>
 * Date created: Dec 1, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class MatchBuilder
{
	private int homeGames;
	private int awayGames;
	
	private TeamOpponents opponents;
	private ArrayList<Match> matchList = new ArrayList<>();
	
	/**
	 * 
	 * Takes a team's opponent list as a parameter.        
	 *
	 * <hr>
	 * Date created: Dec 3, 2020 
	 *
	 * 
	 * @param opponents
	 */
	public MatchBuilder(TeamOpponents opponents)
	{
		this.opponents = opponents;
	}
	
	public MatchBuilder()
	{
		
	}
	
	/**
	 * 
	 * Builds a match from the two teams specified. If values of homeGames or awayGames != 6, home or away is random.         
	 *
	 * <hr>
	 * Date created: Dec 3, 2020
	 *
	 * <hr>
	 * @param team1
	 * @param team2
	 * @return
	 */
	public Match buildMatch(Team team1, Team team2)
	{
		Match match = null;
		Random ran = new Random();
		boolean home = ran.nextBoolean ( );
		if(team2.getTriCode ( ).equals ("BYE"))
		{
			match = new Match(team2, team1);
		}
		else
		{
			if(homeGames == 6 || awayGames == 6)
			{
				if(homeGames == 6)
				{
					match = new Match(team1, team2);
					awayGames++;
				}
				else if(awayGames == 6)
				{
					match = new Match(team2, team1);
					homeGames++;
				}			
			}
			else
			{
				match = (home) ? new Match(team2, team1) : new Match(team1, team2);			
			}			
		}
		return match;
	}
	
	/**
	 * 
	 * Adds a match to the match list.         
	 *
	 * <hr>
	 * Date created: Dec 3, 2020
	 *
	 * <hr>
	 * @param match
	 */
	public void addMatch(Match match)
	{
		matchList.add (match);
	}
	
	/**
	 * 
	 * Creates matches from a team's opponents list and adds them to the match list.         
	 *
	 * <hr>
	 * Date created: Dec 3, 2020
	 *
	 * <hr>
	 */
	public void initializeMatches()
	{
		ArrayList<Team> teamOpponents = new ArrayList<>(opponents.getOpponentList ( ).stream ( ).filter (t -> !t.getTriCode ( ).equals ("BYE")).collect (Collectors.toList ( )));
		Team team1 = opponents.getTeam ( );
		for(int i = 0; i < teamOpponents.size ( ); i++)
		{
			addMatch(buildMatch(team1, teamOpponents.get (i)));
		}
		Team byeWeek = opponents.getOpponentList ( ).stream ( ).filter (t -> t.getTriCode ( ).equals ("BYE")).findFirst().orElse (null);
		addMatch(buildMatch(team1, byeWeek));
		shuffleMatchList();
	}
	
	public void printMatchList()
	{
		for(int i = 0; i < matchList.size ( ); i++)
		{
			System.out.println(matchList.get (i).toString ( ));
		}
	}
	
	public ArrayList<Match> getMatchList()
	{
		return matchList;
	}
	
	private void shuffleMatchList()
	{
		Collections.shuffle (matchList);
		if(matchList.get ((matchList.size ( )) - 1).getTeam2 ( ).getTriCode ( ).equals ("BYE") || matchList.get (0).getTeam2 ( ).getTriCode ( ).equals ("BYE"))
		{
			Collections.shuffle (matchList);
		}
	}
}
