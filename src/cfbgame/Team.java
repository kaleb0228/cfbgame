/**
 * ---------------------------------------------------------------------------
 * File name: Team.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 17, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.awt.Color;
import java.util.ArrayList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 17, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class Team extends Organization
{
	private int rivalryWeek;
	
	private String mascot;
	private String stadium;
	private String city;
	private String state;
	private String conference;
	private String division;
	
	private Color primaryColor;
	private Color secondaryColor;
	private Color tertiaryColor;
	
	private TeamScheduleProperties tsp;
	
	public Team()
	{
		super();
		rivalryWeek = 14;
		mascot = "Football Team";
		stadium = "No Stadium";
		city = "No City";
		state = "No State";
		conference = "No Conference";
		division = "No Division";
		primaryColor = Color.RED;
		secondaryColor = Color.YELLOW;
		tertiaryColor = Color.WHITE;
		tsp = new TeamScheduleProperties(0,0,0);
	}
	
	public Team(String name, String triCode, String mascot, String stadium, String city, String state, int rating) throws Exception
	{
		super(name, triCode, rating);
		this.mascot = mascot;
		this.stadium = stadium;
		this.city = city;
		this.state = state;
		tsp = new TeamScheduleProperties(0,0,0);
	}
	
	public void copyTeam(Team team)
	{
		setName(team.getName ( ));
		setTriCode(team.getTriCode ( ));
		setStadium(team.getStadium ( ));
		setCity(team.getCity ( ));
		setState(team.getState ( ));
		setRating(team.getRating ( ));
	}
	
	public void setPrimaryColor(Color primaryColor)
	{
		this.primaryColor = primaryColor;
	}
	
	public Color getPrimaryColor()
	{
		return primaryColor;
	}
	
	public void setSecondaryColor(Color secondaryColor)
	{
		this.secondaryColor = secondaryColor;
	}
	
	public Color getSecondaryColor()
	{
		return secondaryColor;
	}	
	
	public void setTertiaryColor(Color tertiaryColor)
	{
		this.tertiaryColor = tertiaryColor;
	}
	
	public Color getTertiaryColor()
	{
		return tertiaryColor;
	}	
	
	public void setMascot(String mascot)
	{
		this.mascot = mascot;
	}
	
	public String getMascot()
	{
		return mascot;
	}
	
	public void setConference(String conferenceName)
	{
		this.conference = conferenceName;
	}
	
	public String getConference()
	{
		return conference;
	}

	public void setTriCode (String triCode)
	{
		this.triCode = triCode;
	}

	public String getStadium ( )
	{
		return stadium;
	}

	public void setStadium (String stadium)
	{
		this.stadium = stadium;
	}

	public String getCity ( )
	{
		return city;
	}

	public void setCity (String city)
	{
		this.city = city;
	}

	public String getState ( )
	{
		return state;
	}

	public void setState (String state)
	{
		this.state = state;
	}
	
	public String getDivision()
	{
		return division;
	}
	
	public void setDivision(String division)
	{
		this.division = division;
	}
	
	public String getConferenceName(String tricode, ArrayList<Conference> conferenceList)
	{
		String conferenceName = new String();
		for(int i = 0; i < conferenceList.size ( ); i++)
		{
			if(triCode.equals (conferenceList.get (i).getTriCode ( )))
			{
				conferenceName = conferenceList.get (i).getName ( );
			}
		}
		return conferenceName;		
	}
	
	public void setRivalryWeek (int rivalryWeek)
	{
		this.rivalryWeek = rivalryWeek;
	}
	
	public int getRivalryWeek ( )
	{
		return rivalryWeek;
	}

	/**
	 * Generates hashCode         
	 *
	 * <hr>
	 * Date created: Dec 4, 2020 
	 *
	 * <hr>
	 * @return
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode ( )
	{
		final int prime = 31;
		int result = super.hashCode ( );
		result = prime * result + ( (city == null) ? 0 : city.hashCode ( ));
		result = prime * result + ( (conference == null) ? 0 : conference.hashCode ( ));
		result = prime * result + ( (division == null) ? 0 : division.hashCode ( ));
		result = prime * result + ( (mascot == null) ? 0 : mascot.hashCode ( ));
		result = prime * result + ( (primaryColor == null) ? 0 : primaryColor.hashCode ( ));
		result = prime * result + ( (secondaryColor == null) ? 0 : secondaryColor.hashCode ( ));
		result = prime * result + ( (stadium == null) ? 0 : stadium.hashCode ( ));
		result = prime * result + ( (state == null) ? 0 : state.hashCode ( ));
		result = prime * result + ( (tertiaryColor == null) ? 0 : tertiaryColor.hashCode ( ));
		return result;
	}

	/**
	 * Override of Object.equals()         
	 *
	 * <hr>
	 * Date created: Dec 4, 2020 
	 *
	 * <hr>
	 * @param obj
	 * @return
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (Object obj)
	{
		if (this == obj)
			return true;
		if ( !super.equals (obj))
			return false;
		if (getClass ( ) != obj.getClass ( ))
			return false;
		Team other = (Team) obj;
		if (city == null)
		{
			if (other.city != null)
				return false;
		}
		else if ( !city.equals (other.city))
			return false;
		if (conference == null)
		{
			if (other.conference != null)
				return false;
		}
		else if ( !conference.equals (other.conference))
			return false;
		if (division == null)
		{
			if (other.division != null)
				return false;
		}
		else if ( !division.equals (other.division))
			return false;
		if (mascot == null)
		{
			if (other.mascot != null)
				return false;
		}
		else if ( !mascot.equals (other.mascot))
			return false;
		if (primaryColor == null)
		{
			if (other.primaryColor != null)
				return false;
		}
		else if ( !primaryColor.equals (other.primaryColor))
			return false;
		if (secondaryColor == null)
		{
			if (other.secondaryColor != null)
				return false;
		}
		else if ( !secondaryColor.equals (other.secondaryColor))
			return false;
		if (stadium == null)
		{
			if (other.stadium != null)
				return false;
		}
		else if ( !stadium.equals (other.stadium))
			return false;
		if (state == null)
		{
			if (other.state != null)
				return false;
		}
		else if ( !state.equals (other.state))
			return false;
		if (tertiaryColor == null)
		{
			if (other.tertiaryColor != null)
				return false;
		}
		else if ( !tertiaryColor.equals (other.tertiaryColor))
			return false;
		return true;
	}

	@Override
	public String toString ( )
	{
		return "Team Name:" + name + "\nTeam ID: " + triCode + "\nStadium: " + stadium + "\nCity: " + city + "\nState: " +
						state + "\nRating: " + rating;
	}
	
	private class TeamScheduleProperties
	{
		private int consecutiveGamesPlayed;
		private int conferenceGamesPlayed;
		private int outOfConferenceGamesPlayed;
		
		public TeamScheduleProperties(int consecGamesPlayed, int confGamesPlayed, int RIVALRY_WEEK)
		{
			this.consecutiveGamesPlayed = consecGamesPlayed;
			this.conferenceGamesPlayed = confGamesPlayed;
		}

		
		/**
		 * @return consecutiveGamesPlayed
		 */
		public int getConsecutiveGamesPlayed ( )
		{
			return consecutiveGamesPlayed;
		}

		
		/**
		 * @param consecutiveGamesPlayed the consecutiveGamesPlayed to set
		 */
		public void setConsecutiveGamesPlayed (int consecutiveGamesPlayed)
		{
			this.consecutiveGamesPlayed = consecutiveGamesPlayed;
		}

		
		/**
		 * @return conferenceGamesPlayed
		 */
		public int getConferenceGamesPlayed ( )
		{
			return conferenceGamesPlayed;
		}

		
		/**
		 * @param conferenceGamesPlayed the conferenceGamesPlayed to set
		 */
		public void setConferenceGamesPlayed (int conferenceGamesPlayed)
		{
			this.conferenceGamesPlayed = conferenceGamesPlayed;
		}
		
		public void addConsecutiveGame ( )
		{
			consecutiveGamesPlayed++;
		}
		
		public void addConferenceGame ( )
		{
			conferenceGamesPlayed++;
		}
		
		public void resetConsGameCounter ( )
		{
			consecutiveGamesPlayed = 0;
		}
	}
}
