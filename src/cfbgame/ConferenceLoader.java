/**
 * ---------------------------------------------------------------------------
 * File name: ConferenceLoader.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course: CSCI 1260
 * Creation Date: Nov 18, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Loads conference data and populates it into an ArrayList of Conferences.
 *
 * <hr>
 * Date created: Nov 18, 2020
 * <hr>
 * 
 * @author Kaleb Dippold
 */
public class ConferenceLoader extends DataLoader
{
	private String					filePath;
	private ArrayList <String>		conferenceFile	= new ArrayList <String> ( );
	//private DefinitionsMediator		mediator;
	private ArrayList <Conference>	conferenceList;
	private ConferenceAssignmentLoader cal;

	public ConferenceLoader (String filePath)
	{
		super (filePath);
		this.filePath = filePath;
	}

	/**
	 * 
	 * Uses a FileReader wrapped in a BufferedReader in order to create a String[] of every line in the input file.
	 *
	 * <hr>
	 * Date created: Nov 18, 2020
	 *
	 * <hr>
	 * 
	 * @see cfbgame.DataLoader#loadFile()
	 */
	@Override
	public void loadFile ( ) throws FileNotFoundException, IOException
	{
		BufferedReader confReader = new BufferedReader (new FileReader (filePath), 16384);
		StringBuilder confLine = new StringBuilder ( );
		String line;

		while ( (line = confReader.readLine ( )) != null)
		{
			if ( !line.startsWith ("#"))
			{
				confLine.append (line);
				confLine.append (System.lineSeparator ( ));
				conferenceFile.add (line);
			}
		}
		confReader.close ( );
	}
	
	/**
	 * 
	 * Populates the conference array from the String[] conferenceFile.
	 *
	 * <hr>
	 * Date created: Nov 18, 2020
	 *
	 * <hr>
	 * 
	 * @see cfbgame.DataLoader#populate()
	 */
	public void populate ( )
	{
		conferenceList = new ArrayList<Conference>();
		System.out.println ("Initializing conferences...");
		for (int i = 0; i < conferenceFile.size ( ); i++ )
		{
			ArrayList <String> al = new ArrayList <String> (Arrays.asList (conferenceFile.get (i).split ("\\|"))); // Convert resulting list															 // into arrayList.
			Conference conference = new Conference ( );
			conference.setTricode (al.get (0)); // Tricode
			conference.setName (al.get (1)); // Name
			conference.setRating (Integer.valueOf (al.get (2))); // Rating
			conferenceList.add (conference); // Add to conferenceList.
		}
		System.out.println ("Initialized conference " + conferenceList.size ( ) + " conferences successfully.");
	}

	
	/**
	 * @return conferenceList
	 */
	public ArrayList<Conference> getConferenceList ( )
	{
		return conferenceList;
	}
	
	/**
	 * 
	 * Find a conference from the conference list given the triCode id.         
	 *
	 * <hr>
	 * Date created: Nov 26, 2020
	 *
	 * <hr>
	 * @param triCode
	 * @return
	 */
	public Conference findConference(String triCode)
	{
		Conference conference = new Conference();
		for(int i = 0; i < conferenceList.size ( ); i++)
		{
			if(triCode.equals(conferenceList.get (i).getTriCode ( )))
			{
				conference = conferenceList.get (i);
			}
		}
		return conference;			
	}
	
	public void loadConferenceAssignments(ArrayList<Team> teamList, String triCode)
	{
		cal = new ConferenceAssignmentLoader(filePath, teamList);
		cal.setFilePath (triCode);
		try
		{
			cal.loadFile ( );
			cal.populate ( );
		}
		catch(FileNotFoundException e)
		{
			System.out.println (e.getMessage ( ));
		}
		catch(IOException e)
		{
			System.out.println (e.getMessage ( ));
		}
	}
	
	private class ConferenceAssignmentLoader extends DataLoader
	{
		private String conferenceTriCode;
		private String teamTriCode;
		private String userDirectory = System.getProperty ("user.dir");
		private String filePath;
		
		private ArrayList<Team> teamList;
		private ArrayList<String> caFile = new ArrayList<>();
		
		public ConferenceAssignmentLoader(String filePath, ArrayList<Team> teamList)
		{
			super(filePath);
			this.teamList = teamList;
		}
		
		public void setFilePath(String confTriCode)
		{
			this.filePath = (userDirectory + "\\definitions\\Conferences\\" + confTriCode + ".txt");		
			this.conferenceTriCode = confTriCode;
		}
		
		public void loadFile() throws FileNotFoundException, IOException
		{
			BufferedReader caReader = new BufferedReader(new FileReader(filePath), 16384);
			StringBuilder caLine = new StringBuilder();
			String line;
			
			while((line = caReader.readLine ( )) != null)
			{
				if(!line.startsWith ("#"))
				{
					caLine.append (line);
					caLine.append (System.lineSeparator ( ));
					caFile.add (line);					
				}
			}
			caReader.close ( );		
		}
		
		public void populate()
		{
			for(int i = 0; i < caFile.size ( ); i++)
			{
				teamTriCode = caFile.get (i);
				findConference (conferenceTriCode).addMember (findTeam(teamTriCode));
				setTeamConference(teamTriCode, conferenceTriCode);
			}
		}
		
		public void setTeamConference(String tc, String ctc)
		{
			for(int i = 0; i < teamList.size ( ); i++)
			{
				if(teamList.get (i).getTriCode ( ).equalsIgnoreCase (tc))
				{
					teamList.get (i).setConference (ctc);
				}
			}
		}		
		
		public Team findTeam(String triCode)
		{
			Team teamToFind = new Team();
			for(int i = 0; i < teamList.size ( ); i++)
			{
				if(triCode.equals (teamList.get (i).getTriCode ( )))
				{
					teamToFind = teamList.get (i);
				}
			}
			return teamToFind;			
		}
	}
}
