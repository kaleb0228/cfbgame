/**
 * ---------------------------------------------------------------------------
 * File name: CoachAssignment.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 24, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.HashMap;

/**
 * Maps head coaches, offensive coordinators, and defensive coordinators to teams.
 *
 * <hr>
 * Date created: Nov 24, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public final class CoachAssignment
{
	HashMap<Team, HeadCoach> teamHc = new HashMap<>();
	HashMap<Team, OffCoordinator> teamOc = new HashMap<>();
	HashMap<Team, DefCoordinator> teamDc = new HashMap<>();

	public CoachAssignment(HashMap<Team, HeadCoach> hcMap, HashMap<Team, OffCoordinator> ocMap, HashMap<Team, DefCoordinator> dcMap)
	{
		this.teamHc = hcMap;
		this.teamOc = ocMap;
		this.teamDc = dcMap;
	}
	
	public void assignHc(Team team, HeadCoach coach)
	{
		teamHc.put (team, coach);
	}
	
	public void assignOc(Team team, OffCoordinator oc)
	{
		teamOc.put (team, oc);
	}
	
	public void assignDc(Team team, DefCoordinator dc)
	{
		teamDc.put (team, dc);
	}
	
	public HashMap<Team, HeadCoach> getTeamHcList()
	{
		return teamHc;
	}
	
	public HashMap<Team, OffCoordinator> getTeamOcList()
	{
		return teamOc;
	}
	
	public HashMap<Team, DefCoordinator> getTeamDcList()
	{
		return teamDc;
	}
}
