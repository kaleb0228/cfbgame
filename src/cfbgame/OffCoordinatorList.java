/**
 * ---------------------------------------------------------------------------
 * File name: OffCoordinatorList.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 24, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 24, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class OffCoordinatorList
{
	private ArrayList<OffCoordinator> ocList = new ArrayList<>();
	
	public OffCoordinatorList()
	{
		
	}
	
	public ArrayList<OffCoordinator> getOcList()
	{
		return ocList;
	}
	
	public void setOcList(ArrayList<OffCoordinator> ocList)
	{
		this.ocList = ocList;
	}
}
