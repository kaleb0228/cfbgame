/**
 * ---------------------------------------------------------------------------
 * File name: TeamSchedule.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course: CSCI 1260
 * Creation Date: Nov 27, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Populates the ArrayList<Match> schedule for every team.
 *
 * <hr>
 * Date created: Nov 27, 2020
 * <hr>
 * 
 * @author Kaleb Dippold
 */
public class TeamOpponents
{
	private int								slotsFilled;
	private int								homeGames;
	private int								awayGames;
	private boolean							marqueeOoc;
	private boolean							regionOoc;

	private Team							team;
	private Team							oocRival;
	private Team							crossDivisionRival;
	private Division						division;
	private Division						oppositeDivision;
	private TeamList						teamList;
	private DivisionList					divisionList;
	private RivalryList						rivalryList;
	private ArrayList <Team>				teamArray;
	private ArrayList <List <Team>>			scheduleList		= new ArrayList <> ( );
	private ArrayList <Team>				schedule			= new ArrayList <> ( );
	private scheduleOutput					so;
	private boolean							independentSchedule	= false;

	public TeamOpponents (TeamList tl, DivisionList dl, RivalryList rl)
	{
		this.teamList = tl;
		this.divisionList = dl;
		this.rivalryList = rl;
	}

	public void setTeam (Team team)
	{
		this.team = team;
	}

	public void setTeamList ( )
	{
		teamArray = new ArrayList <> (teamList.getTeamList (team.getTriCode ( )));
	}

	/**
	 * 
	 * Filling out opponents list for every team.
	 *
	 * <hr>
	 * Date created: Nov 29, 2020
	 *
	 * <hr>
	 */
	public void populateSchedule ( )
	{
		boolean oocRivalAdded;
		setTeamList ( );
		getOocRival (rivalryList);
		if (oocRival != null)
		{
			addToSchedule (oocRival);
			removeOpponent (oocRival, teamArray);
		}
		getCrossDivisionRival (rivalryList);
		addToSchedule (crossDivisionRival);
		getDivision ( );
		getOppositeDivision ( );
		addAnnualGames (division.getDivisionMembers (team.getTriCode ( )), schedule);
		addToSchedule (getRandomCrossDivisionTeam (oppositeDivision.getDivisionMembers ( )));
		removeConferenceOpponents (team, teamArray);
		int slotsToFill = 13 - (slotsFilled);
		do
		{
			addAnnualGames (getRandomOocOpps (teamArray, slotsToFill), schedule);
		}
		while (slotsFilled < 13);
		List <Team> distinctSchedule = schedule.stream ( ).distinct ( ).collect (Collectors.toList ( ));
		ArrayList<Team> finalSchedule = new ArrayList<>(distinctSchedule);
		printSchedule (finalSchedule);
		if (finalSchedule.size ( ) == 13)
		{
			finalSchedule.remove (finalSchedule.size ( ) - 1);
		}
		scheduleList.add (finalSchedule);
		writeDebugFile(finalSchedule);

	}

	private void addToSchedule (Team team)
	{
		schedule.add (team);
		slotsFilled++ ;
	}

	public void writeDebugFile (List<Team> schedule)
	{
		so = new scheduleOutput (team.getTriCode ( ));
		so.writeFile (schedule);
	}

	/**
	 * 
	 * Gets the division the team is a member of.
	 *
	 * <hr>
	 * Date created: Nov 28, 2020
	 *
	 * <hr>
	 * 
	 * @param dl
	 */
	private void setDivision ( )
	{
		ArrayList <Division> divList = new ArrayList <> (divisionList.getDivisionList ( ));
		for (int i = 0; i < divList.size ( ); i++ )
		{
			if (team.getDivision ( ).equals (divList.get (i).getTriCode ( )))
			{
				division = divList.get (i);
			}
		}
	}

	public void addAnnualGames (ArrayList <Team> opponents, ArrayList <Team> schedule)
	{
		for (int i = 0; i < opponents.size ( ); i++ )
		{
			schedule.add (opponents.get (i));
			slotsFilled++ ;
		}
	}

	/**
	 * 
	 * For teams in a conference, gets the team's annual cross-division rival.
	 *
	 * <hr>
	 * Date created: Nov 28, 2020
	 *
	 * <hr>
	 * 
	 * @param rl
	 */
	private void getCrossDivisionRival (RivalryList rl)
	{
		this.crossDivisionRival = rl.getCrossDivisionRivalryMap ( ).get (team);
	}

	/**
	 * 
	 * Gets the team's annual out-of-conference rival if it has one.
	 *
	 * <hr>
	 * Date created: Nov 28, 2020
	 *
	 * <hr>
	 * 
	 * @param rl
	 */
	public Team getOocRival (RivalryList rl)
	{
		Team rival = rl.getOocRivalryMap ( ).get (team);
		return rival;
	}

	/**
	 * 
	 * Get a random list of preferred out-of-conference opponents for a team. Provides for a certain chance of adding a "marquee" out of conference game (team of similar
	 * or better prestige)         
	 *
	 * <hr>
	 * Date created: Dec 1, 2020
	 *
	 * <hr>
	 * @param teamList
	 * @param slotsToFill
	 * @return
	 */
	private ArrayList <Team> getRandomOocOpps (ArrayList <Team> teamList, int slotsToFill)
	{
		ArrayList <Team> oocOpps = new ArrayList <> ( );
		Random ran = new Random ( );
		Collections.shuffle (teamList);
		double d = ran.nextDouble ( );
		if (team.getRating ( ) <= 50)
		{
			if (d <= 0.004)
			{
				this.marqueeOoc = true;
			}
		}
		else if (team.getRating ( ) <= 60 && team.getRating ( ) >= 51)
		{
			if (d <= 0.013)
			{
				this.marqueeOoc = true;
			}
		}
		else if (team.getRating ( ) <= 65 && team.getRating ( ) >= 61)
		{
			if (d <= 0.04)
			{
				this.marqueeOoc = true;
			}
		}
		else if (team.getRating ( ) <= 70 && team.getRating ( ) >= 66)
		{
			if (d <= 0.05)
			{
				this.marqueeOoc = true;
			}
		}
		else if (team.getRating ( ) <= 80 && team.getRating ( ) >= 71)
		{
			if (d <= 0.1)
			{
				this.marqueeOoc = true;
			}
		}
		else if (team.getRating ( ) <= 90 && team.getRating ( ) >= 81)
		{
			if (d <= 0.2)
			{
				this.marqueeOoc = true;
			}
		}
		else if (team.getRating ( ) <= 100 && team.getRating ( ) >= 91)
		{
			if (d <= 0.333)
			{
				this.marqueeOoc = true;
			}
		}
		if (marqueeOoc == true)
		{
			if(oocRival != null)
			{
				List <Team> marqueeOpponentsList = teamList.stream ( )
								.filter (t -> t.getRating ( ) > team.getRating ( ) - 20)
								.filter (t -> !t.getTriCode ( ).equals (oocRival.getTriCode ( )))
								.filter (t -> !t.getTriCode ( ).equals ("BYE"))
								.filter (t -> !t.getConference ( ).equals (team.getConference ( )))
								.collect (Collectors.toList ( ));
				Collections.shuffle (marqueeOpponentsList);
				oocOpps.add (marqueeOpponentsList.get (0));
				slotsToFill--;
			}
			else
			{
				List <Team> marqueeOpponentsList = teamList.stream ( )
								.filter (t -> t.getRating ( ) > team.getRating ( ) - 20)
								.filter (t -> !t.getTriCode ( ).equals ("BYE"))
								.filter (t -> !t.getConference ( ).equals (team.getConference ( )))
								.collect (Collectors.toList ( ));
				Collections.shuffle (marqueeOpponentsList);
				oocOpps.add (marqueeOpponentsList.get (0));	
				slotsToFill--;
			}
		}
		for (int i = 0; i < slotsToFill; i++ )
		{
			if (oocRival != null)
			{
				List <Team> oocOpponentsList = teamList.stream ( )
								.filter (t -> !t.getTriCode ( ).equals (oocRival.getTriCode ( )))
								.filter (t -> !t.getTriCode ( ).equals ("BYE"))
								.filter (t -> !t.getConference ( ).equals (team.getConference ( )))
								.collect (Collectors.toList ( ));
				Collections.shuffle (oocOpponentsList);
				oocOpps.add (oocOpponentsList.get (i));
			}
			else
			{
				List <Team> oocOpponentsList = teamList.stream ( )
								.filter (t -> !t.getTriCode ( ).equals ("BYE"))
								.filter (t -> !t.getConference ( ).equals (team.getConference ( )))
								.collect (Collectors.toList ( ));
				Collections.shuffle (oocOpponentsList);
				oocOpps.add (oocOpponentsList.get (i));
			}
		}
		return oocOpps;
	}

	/**
	 * 
	 * Filters the team's opposite division through a stream and gets a random cross-division opponent.         
	 *
	 * <hr>
	 * Date created: Dec 1, 2020
	 *
	 * <hr>
	 * @param oppositeDivision
	 * @return
	 */
	public Team getRandomCrossDivisionTeam (ArrayList <Team> oppositeDivision)
	{
		List <Team> filteredOppositeDivision = oppositeDivision.stream ( )
						.filter (t -> !t.getTriCode ( ).equals (crossDivisionRival.getTriCode ( )))
						.filter (t -> !t.getTriCode ( ).equals ("BYE"))
						.collect (Collectors.toList ( ));
		Collections.shuffle (filteredOppositeDivision);

		return filteredOppositeDivision.get (0);
	}
	
	public Team getRandomDivisionTeam(ArrayList<Team> division)
	{
		List<Team> filteredDivision = division.stream ( )
						.filter (t -> !t.getTriCode ( ).equals (team.getTriCode ( )))
						.collect (Collectors.toList ( ));
		Collections.shuffle (filteredDivision);
		
		return filteredDivision.get (0);
	}

	/**
	 * 
	 * Gets the opposite division by searching through the division list for a division that is of the same conference
	 * but does not share the triCode in the team's getDivision()
	 *
	 * <hr>
	 * Date created: Nov 28, 2020
	 *
	 * <hr>
	 */
	private void setOppositeDivision ( )
	{
		ArrayList <Division> divList = new ArrayList <> (divisionList.getDivisionList ( ));
		for (int i = 0; i < divList.size ( ); i++ )
		{
			if (divList.get (i).getConference ( ).equals (team.getConference ( )) &&
							!divList.get (i).getTriCode ( ).equals (team.getDivision ( )))
			{
				oppositeDivision = divList.get (i);
			}
		}
	}

	/**
	 * 
	 * Removes an opponent from the teamList (used in order to avoid duplicates in schedule list.)         
	 *
	 * <hr>
	 * Date created: Dec 1, 2020
	 *
	 * <hr>
	 * @param team
	 * @param teamList
	 */
	private void removeOpponent (Team team, ArrayList <Team> teamList)
	{
		for (int i = 0; i < teamList.size ( ); i++ )
		{
			if (teamList.get (i).getTriCode ( ).equals (team.getTriCode ( )))
			{
				teamList.remove (i);
			}
		}
	}

	/**
	 * 
	 * Removes conference opponents from teamList (to avoid duplicates.)         
	 *
	 * <hr>
	 * Date created: Dec 1, 2020
	 *
	 * <hr>
	 * @param team
	 * @param teamList
	 */
	private void removeConferenceOpponents (Team team, ArrayList <Team> teamList)
	{
		for (int i = 0; i < teamList.size ( ); i++ )
		{
			if (teamList.get (i).getConference ( ).equals (team.getConference ( )))
			{
				teamList.remove (i);
			}
		}
	}

	public void addGame ( )
	{
		slotsFilled++ ;
	}

	public void printSchedule (List <Team> schedule)
	{
		System.out.println ("Schedule for: " + team.getName ( ));
		for (int i = 0; i < schedule.size ( ); i++ )
		{
			System.out.println ("Game " + (i + 1) + ": " + schedule.get (i).getName ( ));
		}
	}

	public ArrayList <List <Team>> getScheduleList ( )
	{
		return scheduleList;
	}

	/**
	 * 
	 * If home = true, team will schedule as a home game, if home = false, team will play on the road.
	 *
	 * <hr>
	 * Date created: Nov 30, 2020
	 *
	 * <hr>
	 * 
	 * @return
	 */
	public boolean homeOrAway ( )
	{
		Random ran = new Random ( );
		boolean homeOrAway;
		if (homeGames == 6)
		{
			homeOrAway = false;
			awayGames++ ;
		}
		else if (awayGames == 6)
		{
			homeOrAway = true;
			homeGames++ ;
		}
		else
		{
			homeOrAway = ran.nextBoolean ( );
			if (homeOrAway == false)
			{
				awayGames++ ;
			}
			else
			{
				homeGames++ ;
			}
		}
		return homeOrAway;
	}

	public ArrayList <Team> getOpponentList ( )
	{
		return schedule;
	}
	
	public Division getOppositeDivision()
	{
		return oppositeDivision;
	}
	
	public Division getDivision()
	{
		return division;
	}
	
	public Team getRandomOutOfConferenceTeam(ArrayList<Team> teamList)
	{
		List<Team> outOfConferenceTeams = teamList.stream ( ).filter (t -> !t.getConference ( ).equals (team.getConference ( )))
						.collect (Collectors.toList ( ));
		Collections.shuffle (outOfConferenceTeams);
		return outOfConferenceTeams.get (0);
	}

	private class scheduleOutput
	{
		private String	userDirectory	= System.getProperty ("user.dir");
		File			testFile;
		FileWriter		fw;

		private scheduleOutput (String triCode)
		{
			testFile = new File (userDirectory + "//testing//scheduleOutput//" + triCode + "_" + String.valueOf (marqueeOoc) + "_" + String.valueOf (slotsFilled) +  "_schedule.csv");
			try
			{
				fw = new FileWriter (testFile);
			}
			catch (IOException e)
			{
				System.out.println (e.getMessage ( ));
			}
		}

		private void writeFile (List <Team> schedule)
		{
			try
			{
				fw.append ("TEAM");
				fw.append (",");
				for (int i = 0; i < schedule.size ( ); i++ )
				{
					fw.append ("WEEK" + (i + 1));
					fw.append (",");
				}
				fw.append ("MARQUEE_OOC");
				fw.append ("\n");
				fw.append (team.getTriCode ( ));
				fw.append (",");
				for(int i = 0; i < schedule.size ( ); i++)
				{
					fw.append (schedule.get (i).getTriCode ( ));
					fw.append (",");
				}
				fw.append (",");
				fw.append (String.valueOf (marqueeOoc));
				fw.close ( );
			}
			catch (IOException e)
			{
				System.out.println (e.getMessage ( ));
			}
		}
	}
	
	public Team searchOpponentsList(String triCode)
	{
		Team team = null;
		for(Team t : schedule)
		{
			if(t.getTriCode ( ).equals ("HAW"))
			{
				team = t;
			}
		}
		return team;
	}
	
	public Team getTeam()
	{
		return team;
	}
}
