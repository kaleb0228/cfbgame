/**
 * ---------------------------------------------------------------------------
 * File name: TeamScheduleList.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 28, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 28, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class TeamScheduleList
{
	public ArrayList<TeamOpponents> scheduleList = new ArrayList<>();
	
	public ArrayList<TeamOpponents> getScheduleList()
	{
		return scheduleList;
	}
	
	public void setScheduleList(ArrayList<TeamOpponents> scheduleList)
	{
		this.scheduleList = scheduleList;
	}
}
