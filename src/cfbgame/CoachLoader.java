/**
 * ---------------------------------------------------------------------------
 * File name: CoachLoader.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 19, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 19, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class CoachLoader extends DataLoader
{
	private String filePath;
	private String userDir = System.getProperty ("user.dir");
	
	private ArrayList<String> headCoachFile = new ArrayList<>();
	private ArrayList<HeadCoach> headCoachList = new ArrayList<>();
	private ArrayList<String> oCoordinatorFile = new ArrayList<>();
	private ArrayList<OffCoordinator> oCoordinatorList = new ArrayList<>();
	private ArrayList<String> dCoordinatorFile = new ArrayList<>();	
	private ArrayList<DefCoordinator> dCoordinatorList = new ArrayList<>();
	private HashMap<Team, HeadCoach> hcAssignment;
	private HashMap<Team, OffCoordinator> ocAssignment;
	private HashMap<Team, DefCoordinator> dcAssignment;
	
	private TeamList teamList;
	
	CoachAssignmentLoader cal;
	
	public CoachLoader(String filePath)
	{
		super(filePath);
		this.filePath = filePath;
	}
	
	public void setTeamList(TeamList teamList)
	{
		this.teamList = teamList;
	}
	
	/**
	 * 
	 * Loads designated file from definitions folder. Throws IOException if file not found.         
	 *
	 * <hr>
	 * Date created: Nov 19, 2020 
	 *
	 * <hr>
	 * @see cfbgame.DataLoader#loadFile()
	 */
	public void loadFile() throws IOException, FileNotFoundException
	{
		BufferedReader coachReader = new BufferedReader(new FileReader(filePath), 16384);
		StringBuilder headCoachLine = new StringBuilder();
		StringBuilder oCoordinatorLine = new StringBuilder();
		StringBuilder dCoordinatorLine = new StringBuilder();
		String line;
		
		while((line = coachReader.readLine ( )) != null)
		{
			if(!line.startsWith ("#"))
			{
				if(line.contains ("Head Coach"))
				{
					headCoachLine.append (line);
					headCoachLine.append (System.lineSeparator ( ));
					headCoachFile.add (line);							
				}
				else if(line.contains ("OffensiveCoordinator"))
				{
					oCoordinatorLine.append (line);
					oCoordinatorLine.append (System.lineSeparator ( ));
					oCoordinatorFile.add (line);
				}
				else if(line.contains ("DefensiveCoordinator"))
				{
					dCoordinatorLine.append (line);
					dCoordinatorLine.append (System.lineSeparator ( ));
					dCoordinatorFile.add (line);
				}
			}
		}
		coachReader.close ( );
	}
	
	/**
	 * 
	 * Enter method description here         
	 *
	 * <hr>
	 * Date created: Nov 19, 2020 
	 *
	 * <hr>
	 * @see cfbgame.DataLoader#populate()
	 */
	public void populate()
	{
		System.out.println("Initializing head coaches...");
		for(int i = 0; i < headCoachFile.size ( ); i++)
		{
			String[] hcString = headCoachFile.get (i).split ("\\|"); // Split the lines in the conferenceFile list into own strings.
			ArrayList<String> al = new ArrayList<String>(Arrays.asList (hcString)); // Convert resulting list into arrayList.
			HeadCoach headCoach = new HeadCoach(al.get (0), Integer.valueOf (al.get (1)), Integer.valueOf (al.get (2)), CoachTrait.valueOf (al.get (4)), CoachTrait.HeadCoachTrait.valueOf (al.get (5)));
			addCoach(headCoach); // Add to conferenceList.
		}
		
		System.out.println("Initialized " + headCoachList.size ( ) + " head coaches successfully.");
		System.out.println("Initializing offensive coordinators...");
		
		for(int i = 0; i < oCoordinatorFile.size ( ); i++)
		{
			String[] ocString = oCoordinatorFile.get (i).split ("\\|"); // Split the lines in the conferenceFile list into own strings.
			ArrayList<String> al = new ArrayList<String>(Arrays.asList (ocString)); // Convert resulting list into arrayList.
			OffCoordinator oc = new OffCoordinator(al.get (0), Integer.valueOf (al.get (1)), Integer.valueOf (al.get (2)), CoachTrait.valueOf (al.get (4)), CoachTrait.OffCoordinatorTrait.valueOf (al.get (5)));
			addCoach(oc); // Add to conferenceList.
		}
		
		System.out.println("Initialized " + oCoordinatorList.size ( ) + " offensive coordinators successfully.");		
		System.out.println("Initializing defensive coordinators...");
		
		for(int i = 0; i < dCoordinatorFile.size ( ); i++)
		{
			String[] dcString = dCoordinatorFile.get (i).split ("\\|"); // Split the lines in the conferenceFile list into own strings.
			ArrayList<String> al = new ArrayList<String>(Arrays.asList (dcString)); // Convert resulting list into arrayList.
			DefCoordinator dc = new DefCoordinator(al.get (0), Integer.valueOf (al.get (1)), Integer.valueOf (al.get (2)), CoachTrait.valueOf (al.get (4)), CoachTrait.DefCoordinatorTrait.valueOf (al.get (5)));
			addCoach(dc); // Add to conferenceList.
		}
		
		System.out.println("Initialized " + dCoordinatorList.size ( ) + " defensive coordinators successfully.");			
	}
	
	public void addCoach(HeadCoach coach)
	{
		headCoachList.add (coach);
	}
	
	public void addCoach(OffCoordinator coach)
	{
		oCoordinatorList.add (coach);
	}
	
	public void addCoach(DefCoordinator coach)
	{
		dCoordinatorList.add (coach);
	}
	
	public ArrayList<HeadCoach> getHeadCoachList()
	{
		return headCoachList;
	}
	
	public ArrayList<OffCoordinator> getOcList()
	{
		return oCoordinatorList;
	}
	
	public ArrayList<DefCoordinator> getDcList()
	{
		return dCoordinatorList;
	}
	
	public void loadCoachAssignments() throws FileNotFoundException, IOException
	{
		String filePath = (userDir + "\\definitions\\coachAssignments.txt");
		cal = new CoachAssignmentLoader(filePath, headCoachList, oCoordinatorList, dCoordinatorList);
		cal.setTeamList (teamList);
		cal.loadFile ( );
		cal.populate ( );
		hcAssignment = cal.getHcAssignment ( );
		ocAssignment = cal.getOcAssignment ( );
		dcAssignment = cal.getDcAssignment ( );
	}
	
	public HashMap<Team, HeadCoach> getHcAssignment()
	{
		return hcAssignment;
	}
	
	public HashMap<Team, OffCoordinator> getOcAssignment()
	{
		return ocAssignment;
	}
	
	public HashMap<Team, DefCoordinator> getDcAssignment()
	{
		return dcAssignment;
	}
	
	private class CoachAssignmentLoader extends DataLoader
	{
		private ArrayList<String> caFile = new ArrayList<>();
		private ArrayList<HeadCoach> headCoachList;
		private ArrayList<OffCoordinator> offCoordinatorList;
		private ArrayList<DefCoordinator> defCoordinatorList;
		
		private TeamList teamList;
		
		private HashMap<Team, HeadCoach> teamHc = new HashMap<>();
		private HashMap<Team, OffCoordinator> teamOc = new HashMap<>();
		private HashMap<Team, DefCoordinator> teamDc = new HashMap<>();		
		
		public CoachAssignmentLoader(String filePath, ArrayList<HeadCoach> hcList, ArrayList<OffCoordinator> ocList, ArrayList<DefCoordinator> dcList)
		{
			super(filePath);
			this.headCoachList = hcList;
			this.offCoordinatorList = ocList;
			this.defCoordinatorList = dcList;
		}
		
		public void setTeamList(TeamList teamList)
		{
			this.teamList = teamList;
		}
		
		public void loadFile() throws FileNotFoundException, IOException
		{
			BufferedReader caReader = new BufferedReader(new FileReader(filePath), 16384);
			StringBuilder caLine = new StringBuilder();
			String line;
			
			while((line = caReader.readLine ( )) != null)
			{
				if(!line.startsWith ("#"))
				{
					caLine.append (line);
					caLine.append (System.lineSeparator ( ));
					caFile.add (line);					
				}
			}
			caReader.close ( );			
		}
		
		public void populate()
		{
			for(int i = 0; i < caFile.size ( ); i++)
			{
				ArrayList<String> caLine = new ArrayList<>(Arrays.asList (caFile.get (i).split ("\\|")));
				teamHc.put (findTeam(caLine.get (0)), findHeadCoach(caLine.get (1)));
				teamOc.put (findTeam(caLine.get (0)), findOffCoordinator(caLine.get (2)));
				teamDc.put (findTeam(caLine.get (0)), findDefCoordinator(caLine.get (3)));
			}	
		}
		
		public HeadCoach findHeadCoach(String name)
		{
			HeadCoach hc = new HeadCoach();
			for(int i = 0; i < headCoachList.size ( ); i++)
			{
				if(headCoachList.get (i).getName ( ).equals (name))
				{
					return headCoachList.get (i);
				}
			}
			return null;
		}
		
		public OffCoordinator findOffCoordinator(String name)
		{
			for(int i = 0; i < offCoordinatorList.size ( ); i++)
			{
				if(offCoordinatorList.get (i).getName ( ).equals (name))
				{
					return offCoordinatorList.get (i);
				}
			}
			return null;
		}
		
		public DefCoordinator findDefCoordinator(String name)
		{
			for(int i = 0; i < defCoordinatorList.size ( ); i++)
			{
				if(defCoordinatorList.get (i).getName ( ).equals (name))
				{
					return defCoordinatorList.get (i);
				}
			}
			return null;
		}
		
		public HashMap<Team, HeadCoach> getHcAssignment()
		{
			return teamHc;
		}
		
		public HashMap<Team, OffCoordinator> getOcAssignment()
		{
			return teamOc;
		}
		
		public HashMap<Team, DefCoordinator> getDcAssignment()
		{
			return teamDc;
		}
		
		public Team findTeam(String triCode)
		{
			ArrayList<Team> tl = new ArrayList<>(teamList.getTeamList ( ));
			for(int i = 0; i < tl.size ( ); i++)
			{
				if(triCode.equals (tl.get (i).getTriCode ( )))
				{
					return tl.get (i);
				}
			}
			return null;			
		}
	}
}
