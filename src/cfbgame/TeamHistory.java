/**
 * ---------------------------------------------------------------------------
 * File name: TeamHistory.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 19, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

/**
 * Object to hold the historical wins, losses, ties, conference championships, and national championships for a program.
 *
 * <hr>
 * Date created: Nov 19, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class TeamHistory extends History
{
	public TeamHistory()
	{
		super();
	}
	
	public TeamHistory(int wins, int losses, int ties, int confChamps, int natChamps, String triCode)
	{
		super(wins, losses, ties, confChamps, natChamps, triCode);
	}
}
