/**
 * ---------------------------------------------------------------------------
 * File name: EntityMediator.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 23, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 23, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public interface EntityMediator
{
	public void setConferenceListFile(ConferenceList conferenceList);
	public void setTeamListFile(TeamList teamList);
	public ArrayList<Team> getTeamList();	
	public ArrayList<Conference> getConfList();
	public TeamList getTeamListFile();
	public ConferenceList getConferenceListFile();
}
