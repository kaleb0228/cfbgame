/**
 * ---------------------------------------------------------------------------
 * File name: DataLoader.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 18, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Metadata stored in .txt files are loaded into the program and parsed in derived classes.
 *
 * <hr>
 * Date created: Nov 18, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public abstract class DataLoader
{
	protected String filePath;
	protected final String userDirectory = System.getProperty ("user.dir");
	public DataLoader(String filePath)
	{
		this.filePath = filePath;
	}
	
	/**
	 * 
	 * Loads data from a file using a fileReader wrapped inside a bufferedReader, and then adds every line of the file to an array of Strings.         
	 *
	 * <hr>
	 * Date created: Nov 18, 2020
	 *
	 * <hr>
	 * @throws FileNotFoundException, IOException
	 */
	public abstract void loadFile() throws FileNotFoundException, IOException;
	
	/**
	 * 
	 * Populates the data from the String[] of lines into the applicable ArrayList.         
	 *
	 * <hr>
	 * Date created: Nov 18, 2020
	 */
	abstract void populate();
}
