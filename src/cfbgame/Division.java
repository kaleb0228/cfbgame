/**
 * ---------------------------------------------------------------------------
 * File name: Division.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class Division extends Organization
{
	private String conference;
	private ArrayList<Team> divisionMembers = new ArrayList<>();
	
	public Division()
	{
		super();
		conference = "No Conference";
	}
	
	public Division(String name, String triCode, int rating) throws Exception
	{
		super(name, triCode, rating);
	}
	
	public void setConference(String triCode)
	{
		this.conference = triCode;
	}
	
	public void addMember(Team team)
	{
		divisionMembers.add (team);
	}
	
	public String getConference()
	{
		return conference;
	}
	
	public ArrayList<Team> getDivisionMembers()
	{
		return divisionMembers;
	}
	
	/**
	 * 
	 * Returns the division members sans the member with the same triCode as input.         
	 *
	 * <hr>
	 * Date created: Nov 28, 2020
	 *
	 * <hr>
	 * @param triCode
	 * @return
	 */
	public ArrayList<Team> getDivisionMembers(String triCode)
	{
		ArrayList<Team> divMembers = new ArrayList<>();
		for(int i = 0 ; i < divisionMembers.size ( ); i++)
		{
			if(!divisionMembers.get (i).getTriCode ( ).equals (triCode))
			{
				divMembers.add (divisionMembers.get (i));
			}
		}
		return divMembers;
	}
	
	public Division findDivision(String triCode)
	{
		Division division = new Division();
		if(triCode.equals (this.getTriCode ( )))
		{
			division = this;
		}
		return division;
	}
	
	@Override
	public String toString()
	{
		return super.toString ( ) + "\nMembers\n" + divisionMembers.toString ( );
	}
}
