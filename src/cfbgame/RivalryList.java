/**
 * ---------------------------------------------------------------------------
 * File name: RivalryList.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 24, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.HashMap;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;

/**
 * Mapping team rivalries. First map primary rivalries (HashMap), then other rivals (multimap)
 *
 * <hr>
 * Date created: Nov 24, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class RivalryList
{
	private HashMap<Team, Team> primaryRivalryMap = new HashMap<>();
	private HashMap<Team, Team> crossDivisionRivalryMap = new HashMap<>();
	private HashMap<Team, Team> oocRivalryMap = new HashMap<>();
	private ListMultimap<Team, Team> secondaryRivalryMap = ArrayListMultimap.create ( );
	
	public RivalryList()
	{
		
	}
	
	public void assignPrimaryRival(Team team1, Team team2)
	{
		primaryRivalryMap.put (team1, team2);
	}
	
	public void assignCrossDivisionRival(Team team1, Team team2)
	{
		crossDivisionRivalryMap.put (team1, team2);
	}
	
	public void setPrimaryRivalryMap(HashMap<Team, Team> primaryRivalMap)
	{
		this.primaryRivalryMap = primaryRivalMap;
	}
	
	public void setCrossDivisionRivalryMap(HashMap<Team, Team> crossDivisionRivalryMap)
	{
		this.crossDivisionRivalryMap = crossDivisionRivalryMap;
	}
	
	public void setOocRivalryMap(HashMap<Team, Team> oocRivalryMap)
	{
		this.oocRivalryMap = oocRivalryMap;
	}
	
	public HashMap<Team, Team> getPrimaryRivalryMap()
	{
		return primaryRivalryMap;
	}
	
	public HashMap<Team, Team> getCrossDivisionRivalryMap()
	{
		return crossDivisionRivalryMap;
	}
	
	public HashMap<Team, Team> getOocRivalryMap()
	{
		return oocRivalryMap;
	}
	
	public void setSecondaryRivalryMap(ListMultimap<Team, Team> secondaryRivalMap)
	{
		this.secondaryRivalryMap = secondaryRivalMap;
	}
	
	public ListMultimap<Team, Team> getSecondaryRivalryMap()
	{
		return secondaryRivalryMap;
	}
}
