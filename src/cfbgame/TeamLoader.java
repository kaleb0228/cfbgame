/**
 * ---------------------------------------------------------------------------
 * File name: TeamLoader.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 17, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.awt.Color;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import com.google.common.collect.ListMultimap;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 17, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class TeamLoader extends DataLoader
{

	private ArrayList<String> teamFile = new ArrayList<String>();
	private ArrayList<Team> teamList = new ArrayList<Team>();
	private HashMap<Team, Team> primaryRivalryList;
	private HashMap<Team, Team> secondaryRivalryList;
	private HashMap<Team, Team> oocRivalryList;
	private RivalryList rivalryList;
	TeamColorLoader tcl;
	TeamRivalryLoader trl;
	

	/**
	 * @return teamList
	 */
	public ArrayList <Team> getEntity ( )
	{
		return teamList;
	}

	public TeamLoader(String filePath)
	{
		super(filePath);
	}
	
	public void setRivalryList(RivalryList rivalryList)
	{
		this.rivalryList = rivalryList;
	}
	
	public void addTeam(Team team)
	{
		teamList.add (team);
	}
	
	/**
	 * 
	 * Loading the team file and adding every entry to an ArrayList of Strings.         
	 *
	 * <hr>
	 * Date created: Nov 18, 2020
	 *
	 * <hr>
	 * @throws FileNotFoundException, IOException
	 */
	@Override
	public void loadFile() throws FileNotFoundException, IOException
	{
		BufferedReader teamReader = new BufferedReader(new FileReader(filePath), 16384);
		StringBuilder teamLine = new StringBuilder();
		String line;
		
		while((line = teamReader.readLine ( )) != null)
		{
			if(!line.startsWith ("#"))
			{
				teamLine.append (line);
				teamLine.append (System.lineSeparator ( ));
				teamFile.add (line);				
			}
		}
		teamReader.close ( );
	}
	
	public void populate()
	{
		long startTime = System.nanoTime ( );
		System.out.println("Initializing teams...");
		for(int i = 0; i < teamFile.size ( ); i++)
		{
			ArrayList<String> al = new ArrayList<String>(Arrays.asList (teamFile.get (i).split ("\\|")));
			Team team = new Team();
			team.setTriCode (al.get (0));
			team.setName (al.get (1));
			team.setStadium (al.get (2));
			team.setCity (al.get (3));
			team.setState (al.get (4));
			team.setRating (Integer.valueOf (al.get (5)));
			team.setMascot (al.get (6));
			team.setRivalryWeek (Integer.valueOf (al.get (7)));
			addTeam(team);
		}
		long endTime = System.nanoTime ( );
		long duration = (endTime - startTime) / 100000;
		System.out.println("Initialized " + (teamList.size ( )-1) + " teams succesfully. (Took " + duration + "ms.)");	
	}
	
	public void loadTeamColors(String filePath)
	{
		tcl = new TeamColorLoader(filePath);
		try
		{
			tcl.loadFile ( );
			tcl.populate ( );
		}
		catch(FileNotFoundException e)
		{
			System.out.println(e.getMessage ( ));
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage ( ));
		}
	}
	
	public void loadTeamRivalries(String filePath)
	{
		trl = new TeamRivalryLoader(filePath);
		{
			try
			{
				trl.loadFile ( );
				trl.populate ( );
			}
			catch(FileNotFoundException e)
			{
				System.out.println (e.getMessage ( ));
			}
			catch(IOException e)
			{
				System.out.println (e.getMessage ( ));
			}
		}
	}

	public ArrayList <Team> getTeamList ( )
	{
		return teamList;
	}

	public void setTeamList (ArrayList <Team> teamList)
	{
		this.teamList = teamList;
	}
	
	/**
	 * 
	 * Finds a team from the team list by tricode.         
	 *
	 * <hr>
	 * Date created: Nov 19, 2020
	 *
	 * <hr>
	 * @param triCode
	 * @return
	 */
	public Team find(String key)
	{
		for(int i = 0; i < teamList.size ( ); i++)
		{
			if(key.equals (teamList.get (i).getTriCode ( )))
			{
				return teamList.get (i);
			}
		}
		return null;
	}
	
	public HashMap<Team, Team> getPrimaryRivalryList()
	{
		return primaryRivalryList;
	}
	
	public HashMap<Team, Team> getSecondaryRivalryList()
	{
		return secondaryRivalryList;
	}
	
	private class TeamColorLoader extends DataLoader
	{
		private String triCode;
		
		private Color color1;
		private Color color2;
		private Color color3;
		
		private ArrayList<String> colorFile = new ArrayList<>();
		
		public TeamColorLoader(String filePath)
		{
			super(filePath);
		}
		
		public void loadFile() throws FileNotFoundException, IOException
		{
			BufferedReader colorReader = new BufferedReader(new FileReader(filePath), 16384);
			StringBuilder colorLine = new StringBuilder();
			String line;
			
			while((line = colorReader.readLine ( )) != null)
			{
				if(!line.startsWith ("#"))
				{
					colorLine.append (line);
					colorLine.append (System.lineSeparator ( ));
					colorFile.add (line);				
				}
			}
			colorReader.close ( );
		}
		
		public void populate()
		{
			for(int i = 0; i < colorFile.size ( ); i++)
			{
				ArrayList<String> al = new ArrayList<>(Arrays.asList (colorFile.get (i).split ("\\|")));
				triCode = al.get (0);
				if(al.get (4).equals ("Primary"))
				{
					color1 = getColorValue(Integer.valueOf (al.get (1)), Integer.valueOf (al.get (2)), Integer.valueOf (al.get (3)));
				}
				else if(al.get (4).equals ("Secondary"))
				{
					color2 = getColorValue(Integer.valueOf (al.get (1)), Integer.valueOf (al.get (2)), Integer.valueOf (al.get (3)));
				}
				else if(al.get (4).equals ("Tertiary"))
				{
					color3 = getColorValue(Integer.valueOf (al.get (1)), Integer.valueOf (al.get (2)), Integer.valueOf (al.get (3)));
				}
				find(triCode).setPrimaryColor (color1);
				find(triCode).setSecondaryColor (color2);
				find(triCode).setTertiaryColor (color3);
			}
		}
		
		public Color getColorValue(int r, int g, int b)
		{
			float red = ((float)r / 255);
			float green = ((float)g / 255);
			float blue = ((float)b / 255);
			Color color = new Color(red, green, blue);
			return color;
		}
	}
	
	private class TeamRivalryLoader extends DataLoader
	{
		private ArrayList<String> trFile = new ArrayList<>();
		
		public TeamRivalryLoader(String filePath)
		{
			super(filePath);
		}
		
		public void loadFile() throws FileNotFoundException, IOException
		{
			BufferedReader trReader = new BufferedReader(new FileReader(filePath), 16384);
			StringBuilder trLine = new StringBuilder();
			String line;
			
			while((line = trReader.readLine ( )) != null)
			{
				if(!line.startsWith ("#"))
				{
					trLine.append (line);
					trLine.append (System.lineSeparator ( ));
					trFile.add (line);					
				}
			}
			trReader.close ( );
		}
		
		/**
		 * 
		 * Populates the rivalry maps from parsed file.         
		 *
		 * <hr>
		 * Date created: Nov 24, 2020 
		 *
		 * <hr>
		 * @see cfbgame.DataLoader#populate()
		 */
		public void populate()
		{
			primaryRivalryList = rivalryList.getPrimaryRivalryMap ( );
			secondaryRivalryList = rivalryList.getCrossDivisionRivalryMap ( );
			oocRivalryList = rivalryList.getOocRivalryMap ( );
			for(int i = 0; i < trFile.size ( ); i++)
			{
				ArrayList<String> trLine = new ArrayList<>(Arrays.asList (trFile.get (i).split ("\\|")));
				primaryRivalryList.put (find(trLine.get (0)), find(trLine.get (1)));
			    secondaryRivalryList.put (find(trLine.get (0)), find(trLine.get (2)));
			    if(!trLine.get (3).equals ("NONE"))
			    {
			    	oocRivalryList.put (find(trLine.get (0)), find(trLine.get (3)));
			    }
			}
		}	
	}
}
