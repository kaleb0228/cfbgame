/**
 * ---------------------------------------------------------------------------
 * File name: MasterSchedule.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Dec 3, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.List;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * The league's master schedule. Contains a valid schedule for every team where: no teams will play two other teams in a week
 *
 * <hr>
 * Date created: Dec 3, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class MasterSchedule
{
	private ArrayList<ArrayList<Match>> masterSchedule = new ArrayList<>();
	private MatchBuilder matchBuilder;
	private boolean isValidMasterSchedule;
	private MasterScheduleDebugFile msdf;
	
	public MasterSchedule(ArrayList<ArrayList<Match>> masterSchedule)
	{
		this.masterSchedule = masterSchedule;
	}
	
	public void generateDebugFile()
	{
		masterSchedule = transposeSchedule(masterSchedule);
		generateValidMasterSchedule(masterSchedule);
		msdf = new MasterScheduleDebugFile(masterSchedule);
		msdf.printDebugFile ( );
	}
	
	private void addToMasterSchedule(ArrayList<Match> schedule)
	{
		masterSchedule.add (schedule);
	}
	
	public void generateValidMasterSchedule(ArrayList<ArrayList<Match>> masterSchedule)
	{
		boolean validWeek = false;
		do
		{
			for(int i = 0; i < masterSchedule.size ( ); i++)
			{
				for(int j = 0; j < masterSchedule.get (i).size ( ) - 1; j++)
				{
					if(masterSchedule.get (i).get (j).getTeam1 ( ).getTriCode ( ).equals (masterSchedule.get (i).get (j+1).getTeam1 ( ).getTriCode ( ))
									|| masterSchedule.get (i).get (j).getTeam2 ( ).getTriCode ( ).equals (masterSchedule.get (i).get (j+1).getTeam2 ( ).getTriCode ( ))
									|| masterSchedule.get (i).get (j).getTeam1 ( ).getTriCode ( ).equals (masterSchedule.get (i).get (j+1).getTeam2 ( ).getTriCode ( ))
									|| masterSchedule.get (i).get (j).getTeam2 ( ).getTriCode ( ).equals (masterSchedule.get (i).get (j+1).getTeam1 ( ).getTriCode ( )))
					{
						Collections.swap (masterSchedule, i, i-1);
						validWeek = true;
					}
				}
			}
		}while(validWeek==false);
	}
	
	private ArrayList<ArrayList<Match>> transposeSchedule(ArrayList<ArrayList<Match>> masterSchedule)
	{
		ArrayList<ArrayList<Match>> transposedSchedule = new ArrayList<>();
		for(int i = 0; i < masterSchedule.get (0).size ( ); i++)
		{
			transposedSchedule.add (transposeScheduleWeek(masterSchedule, i));
		}
		return transposedSchedule;
	}
	
	private ArrayList<Match> transposeScheduleWeek(ArrayList<ArrayList<Match>> masterSchedule, int week)
	{
		ArrayList<Match> transposedMatchList = new ArrayList<>();
		for(int i = 0; i < masterSchedule.size ( ); i++)
		{
			transposedMatchList.add (masterSchedule.get (i).get (week));				
		}		
		return transposedMatchList;
	}
	
	private class MasterScheduleDebugFile
	{
		String userDirectory = System.getProperty ("user.dir");
		File masterScheduleChart = new File(userDirectory + "//testing//scheduleOutput//masterSchedule" + ".csv");
		ArrayList<ArrayList<Match>> masterSchedule;
		FileWriter fw;
		
		public MasterScheduleDebugFile(ArrayList<ArrayList<Match>> masterSchedule)
		{
			this.masterSchedule = masterSchedule;
			try
			{
				fw = new FileWriter(masterScheduleChart);
			}
			catch(IOException e)
			{
				System.out.println(e.getMessage ( ));
			}
		}
		
		public void printDebugFile()
		{
			try
			{
				fw.append ("TEAM");
				fw.append (",");
				for(int i = 0; i < 14; i++)
				{
					fw.append ("WEEK" + (i+1));
					fw.append (",");
				}
				fw.append (System.getProperty ("line.separator"));
				for(int row = 0; row < masterSchedule.size ( ); row++)
				{
					for(int col = 0; col < masterSchedule.get (row).size ( ); col++)
					{
						fw.append (masterSchedule.get (row).get (col).getTeam1 ( ).getTriCode ( ) + "/" + masterSchedule.get (row).get (col).getTeam2 ( ).getTriCode ( ));
						fw.append (",");
					}
					fw.append (System.getProperty ("line.separator"));
				}
				fw.close ( );
			}
			catch(IOException e)
			{
				System.out.println(e.getMessage ( ));
			}
		}
	}
}
