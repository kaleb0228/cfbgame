/**
 * ---------------------------------------------------------------------------
 * File name: ConferenceDivisionAssignment.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class ConferenceDivisionAssignment
{
	private ConferenceList confList;
	private DivisionList divList;
	
	public ConferenceDivisionAssignment(ConferenceList confList, DivisionList divList)
	{
		this.confList = confList;
		this.divList = divList;
	}
	
	public void assignDivisions()
	{
		for(int i = 0; i < divList.getDivisionList ( ).size ( ); i++)
		{
			for(int j = 0; j < confList.getConfList ( ).size ( ); j++)
			{
				if(divList.getDivisionList ( ).get (i).triCode.equals (confList.getConfList ( ).get (j).getTriCode ( )))
				{
					confList.getConfList ( ).get (j).addDivision (divList.getDivisionList ( ).get (i));
				}				
			}
		}
	}
}
