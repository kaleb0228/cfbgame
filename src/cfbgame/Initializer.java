/**
 * ---------------------------------------------------------------------------
 * File name: Initialization.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 23, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import frontend.CoachInformation;
import frontend.ConferenceInformation;
import frontend.GameInterface;
import frontend.TeamInfoPanel;
import frontend.TeamInformation;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 23, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class Initializer implements Runnable
{
	String filePath = System.getProperty ("user.dir");
	TeamLoader tl = new TeamLoader(filePath + "\\definitions\\teams.txt");
	ConferenceLoader cl = new ConferenceLoader(filePath + "\\definitions\\conferences.txt");
	CoachLoader col = new CoachLoader(filePath + "\\definitions\\coaches.txt");
	DivisionLoader dl = new DivisionLoader(filePath + "\\definitions\\divisions.txt");
	TeamInformation ti;
	ConferenceInformation confInfo;
	CoachInformation ci;
	DefinitionsInitializer di;
	@Override
	public void run ( )
	{	
		loadEntities();
		getInformation(di);
		TeamInfoPanel ip = new TeamInfoPanel(ti, confInfo, ci);
		new GameInterface(ip);
	}
	
	public void loadEntities()
	{
		di = new DefinitionsInitializer(tl, cl, col, dl);
		di.initializeDefinitions ( );
	}
	
	public void getInformation(DefinitionsInitializer di)
	{
		ti = new TeamInformation (di.getTeamList ( ), di.getRivalryList ( ));
		confInfo = new ConferenceInformation(di.getConfList ( ));
		ci = new CoachInformation(di.getHeadCoachList ( ), di.getOffCoordinatorList ( ), di.getDefCoordinatorList ( ), di.getCoachAssignment ( ));
	}
}
