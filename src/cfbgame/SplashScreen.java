/**
 * ---------------------------------------------------------------------------
 * File name: SplashScreen.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 22, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.awt.Color;
import java.awt.Font;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JProgressBar;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 22, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class SplashScreen extends JFrame
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String userDirectory = System.getProperty ("user.dir");
	JFrame splash;
	JLabel image = new JLabel(new ImageIcon(userDirectory + "\\resources\\" + "thanksye.png"));
	JLabel text = new JLabel("Test");
	int i = 0, num = 0;
	JProgressBar progressBar = new JProgressBar();
	
	public SplashScreen()
	{
		splashScreenInit();
		addImage();
		addText();
		addProgressBar();
	}
	
	public void splashScreenInit()
	{
		splash = new JFrame();
		splash.getContentPane ( ).setLayout (null);
		splash.setUndecorated (true);
		splash.setSize (600,400);
		splash.setLocationRelativeTo (null);
		splash.getContentPane ( ).setBackground (Color.WHITE);
		splash.setVisible (true);
	}
	
	public void addImage()
	{
		image.setSize (600,400);
		splash.add (image);
	}
	
	public void addText()
	{
		text.setFont (new Font("arial", Font.BOLD, 30));
		text.setBounds (170,220,600,40);
		text.setForeground (Color.BLUE);
		splash.add (text);
	}
	
	public void addProgressBar()
	{
		progressBar.setBounds (40,40,160,30);
		progressBar.setValue (0);
		progressBar.setStringPainted (true);
		splash.add (progressBar);
	}
	
	public void iterate()
	{
		while(i <= 2000)
		{
			i=i+20;
		}
	}
}
