/**
 * ---------------------------------------------------------------------------
 * File name: CoachHistory.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 20, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Coach win-loss-tied record, conference championships, national championships.
 *
 * <hr>
 * Date created: Nov 20, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class CoachHistory extends History
{
	private String name = new String();
	private ArrayList<String> historyTable = new ArrayList<>();
	
	public CoachHistory()
	{
		super();
		name = "No Name";
	}
	
	public CoachHistory(int wins, int losses, int ties, int confChamps, int natChamps, String triCode, String name)
	{
		super(wins, losses, ties, confChamps, natChamps, triCode);
		this.name = name;
	}
	
	public void addHistoryEntry(String historyEntry)
	{
		historyTable.add (historyEntry);
	}
	
	@Override
	public String toString()
	{
		return "Name: " + name + "\n" + super.toString ( );
	}	
}
