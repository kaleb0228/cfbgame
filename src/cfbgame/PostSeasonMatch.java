/**
 * ---------------------------------------------------------------------------
 * File name: PostSeasonMatch.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 30, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 30, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class PostSeasonMatch extends Match
{
	private boolean bowlGame;
	private boolean confChampGame;
	
	public PostSeasonMatch()
	{
		super();
	}
	
	public PostSeasonMatch(Team awayTeam, Team homeTeam, boolean isBowlGame, boolean isConfChampGame) throws MatchException
	{
		super(awayTeam, homeTeam);
		this.bowlGame = isBowlGame;
		this.confChampGame = isConfChampGame;
		if(isBowlGame == true && isConfChampGame == true)
		{
			throw new MatchException("A match cannot be both a bowl game and a conference championship game!");
		}
	}
}
