/**
 * ---------------------------------------------------------------------------
 * File name: TeamHistoryLoader.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course: CSCI 1260
 * Creation Date: Nov 19, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 19, 2020
 * <hr>
 * 
 * @author Kaleb Dippold
 */
public class TeamHistoryLoader extends DataLoader
{
	private String[] 						historyFiles;
	private String							triCode = new String();
	private ArrayList <String>				thFile	= new ArrayList <> ( );
	private ArrayList <TeamHistory>		    thList	= new ArrayList <> ( );

	public TeamHistoryLoader (String filePath)
	{
		super (filePath);
	}

	/**
	 * 
	 * Enter method description here
	 *
	 * <hr>
	 * Date created: Nov 20, 2020
	 *
	 * <hr>
	 * 
	 * @see cfbgame.DataLoader#loadFile()
	 */
	public void loadFile ( ) throws FileNotFoundException, IOException
	{
		BufferedReader thReader = new BufferedReader (new FileReader (filePath), 16384);
		StringBuilder sb = new StringBuilder ( );
		String line;
		while ( (line = thReader.readLine ( )) != null)
		{
			if ( !line.startsWith ("#"))
			{
				sb.append (line);
				sb.append (System.lineSeparator ( ));
				thFile.add (line);
			}
		}
		thReader.close ( );
	}
	
	/**
	 * 
	 * Overload of loadFile method, but filePath parameter which allows for use of it in a loop.         
	 *
	 * <hr>
	 * Date created: Nov 20, 2020
	 *
	 * <hr>
	 * @param filePath - The path of the file to be parsed.
	 * @return
	 */
	public ArrayList<String> loadFile(String filePath) throws FileNotFoundException, IOException
	{
		ArrayList<String> thFile = new ArrayList<>();
		BufferedReader thReader = new BufferedReader (new FileReader (userDirectory + "\\history\\teams\\" + filePath), 16384);
		StringBuilder sb = new StringBuilder ( );
		String line;
		while ( (line = thReader.readLine ( )) != null)
		{
			if ( !line.startsWith ("#"))
			{
				sb.append (line);
				sb.append (System.lineSeparator ( ));
				thFile.add (line);
			}
		}
		thReader.close ( );
		setTriCode(filePath);
		
		return thFile;
	}

	/**
	 * 
	 * Enter method description here         
	 *
	 * <hr>
	 * Date created: Nov 20, 2020 
	 *
	 * <hr>
	 * @see cfbgame.DataLoader#populate()
	 */
	public void populate()
	{
		int wins = 0;
		int losses = 0;
		int ties = 0;
		int confChamps = 0;
		int natChamps = 0;
		
		wins = lineReader(0, 1, thFile.size ( ));
		losses = lineReader(0, 2, thFile.size ( ));
		ties = lineReader(0, 3, thFile.size ( ));
		confChamps = lineReader(0, 4, thFile.size ( ));
		natChamps = lineReader(0, 5, thFile.size ( ));
		TeamHistory th = new TeamHistory(wins, losses, ties, confChamps, natChamps, "XXX");
		thList.add (th);
			
	}
	
	/**
	 * 
	 * Populate from given thFile.         
	 *
	 * <hr>
	 * Date created: Nov 20, 2020
	 *
	 * <hr>
	 * @param thFile - Parsed file to be used to populate the team history.
	 */
	public void populate(ArrayList<String> thFile)
	{
		int wins = 0;
		int losses = 0;
		int ties = 0;
		int confChamps = 0;
		int natChamps = 0;
		
		wins = lineReader(thFile, 0, 1, thFile.size ( ));
		losses = lineReader(thFile, 0, 2, thFile.size ( ));
		ties = lineReader(thFile, 0, 3, thFile.size ( ));
		confChamps = lineReader(thFile, 0, 4, thFile.size ( ));
		natChamps = lineReader(thFile, 0, 5, thFile.size ( ));
		TeamHistory th = new TeamHistory(wins, losses, ties, confChamps, natChamps, triCode);
		thList.add (th);
	}

	/**
	 * 
	 * Gets value from a given index in a string.         
	 *
	 * <hr>
	 * Date created: Nov 20, 2020
	 *
	 * <hr>
	 * @param startLine - Line in thFile to start with.
	 * @param index - Index of where the attribute is located in the file.
	 * @param length - Length of the collection.
	 * @return
	 */
	public int lineReader (int startLine, int index, int length)
	{
		int a = 0;
		for (int i = startLine; i < length; i++)
		{
			String [ ] thString = thFile.get (i).split ("\\|");
			ArrayList <String> al = new ArrayList <> (Arrays.asList (thString));
			a += Integer.valueOf (al.get (index));
		}
		return a;
	}
	
	/**
	 * 
	 * Overload of lineReader() that takes an ArrayList<String> as a parameter.         
	 *
	 * <hr>
	 * Date created: Nov 20, 2020
	 *
	 * <hr>
	 * @param thFile - File to read.
	 * @param startLine - @see lineReader.
	 * @param index @see lineReader.
	 * @param length @see lineReader.
	 * @return
	 */
	public int lineReader (ArrayList<String> thFile, int startLine, int index, int length)
	{
		int a = 0;
		for (int i = startLine; i < length; i++)
		{
			String [ ] thString = thFile.get (i).split ("\\|");
			ArrayList <String> al = new ArrayList <> (Arrays.asList (thString));
			a += Integer.valueOf (al.get (index));
		}
		return a;
	}
	
	public void setTriCode(String filePath)
	{
		String teamID = filePath;
		int index = teamID.indexOf (" ");
		this.triCode = filePath.substring (0, index);
	}
	
	/**
	 * 
	 * Grabs the list of history files from the team history folder.         
	 *
	 * <hr>
	 * Date created: Nov 20, 2020
	 *
	 * <hr>
	 */
	public void exploreTeamFolder()
	{
		File h = new File(filePath);
		setHistoryFiles (h.list ( ));
	}

	
	/**
	 * @return historyFiles
	 */
	public String[] getHistoryFiles ( )
	{
		return historyFiles;
	}

	
	/**
	 * @param historyFiles the historyFiles to set
	 */
	public void setHistoryFiles (String[] historyFiles)
	{
		this.historyFiles = historyFiles;
	}
}
