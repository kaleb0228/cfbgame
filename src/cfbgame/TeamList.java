/**
 * ---------------------------------------------------------------------------
 * File name: TeamList.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 22, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 22, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public final class TeamList
{
	private ArrayList<Team> teamList = new ArrayList<>();
	
	public ArrayList<Team> getTeamList()
	{
		return teamList;
	}
	
	/**
	 * 
	 * Returns the teamList sans the team with triCode inputted.         
	 *
	 * <hr>
	 * Date created: Nov 28, 2020
	 *
	 * <hr>
	 * @param triCode
	 * @return
	 */
	public ArrayList<Team> getTeamList(String triCode)
	{
		ArrayList<Team> tlist = new ArrayList<>();
		for(int i = 0; i < teamList.size ( ); i++)
		{
			if(!teamList.get (i).getTriCode ( ).equals (triCode))
			{
				tlist.add (teamList.get (i));
			}
		}
		return tlist;
	}
	
	public void setTeamList(ArrayList<Team> teamList)
	{
		this.teamList = teamList;
	}
	
	public Team findTeam(String triCode)
	{
		Team team = null;
		for(int i = 0; i < teamList.size ( ); i++)
		{
			if(teamList.get (i).getTriCode ( ).equals (triCode))
			team = teamList.get (i);
		}
		return team;
	}
}
