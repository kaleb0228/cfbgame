/**
 * ---------------------------------------------------------------------------
 * File name: RegularSeasonMatch.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 30, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;


/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 30, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class RegularSeasonMatch extends Match
{
	private boolean conferenceGame;
	private boolean divisionGame;
	private boolean rivalryGame;
	public RegularSeasonMatch()
	{
		super();
	}
	
	/**
	 * 
	 * Creating a regular season match.        
	 *
	 * <hr>
	 * Date created: Nov 30, 2020 
	 *
	 * 
	 * @param awayTeam
	 * @param homeTeam
	 * @param isConferenceGame
	 * @param isDivisionGame
	 * @param isRivalryGame
	 * @throws MatchException
	 */
	public RegularSeasonMatch(Team awayTeam, Team homeTeam, boolean isConferenceGame, boolean isDivisionGame, boolean isRivalryGame) throws MatchException
	{
		super(awayTeam, homeTeam);
		this.conferenceGame = isConferenceGame;
		this.divisionGame = isDivisionGame;
		this.rivalryGame = isRivalryGame;
		if(isConferenceGame == false && isDivisionGame == true)
		{
			throw new MatchException("Matches marked as division games must also be marked as conference matches!");
		}
	}
	
	public void setConferenceGame(boolean isConferenceGame)
	{
		this.conferenceGame = isConferenceGame;
	}
	
	public void setDivisionGame(boolean isDivisionGame) throws MatchException
	{
		this.divisionGame = isDivisionGame;
		if(conferenceGame == false && isDivisionGame == true)
		{
			throw new MatchException("Matches marked as division games must also be marked as conference matches!");
		}
	}
	
	public void setRivalryGame(boolean isRivalryGame)
	{
		this.rivalryGame = isRivalryGame;
	}
}
