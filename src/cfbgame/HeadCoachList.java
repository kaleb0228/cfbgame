/**
 * ---------------------------------------------------------------------------
 * File name: HeadCoachList.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 24, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 24, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class HeadCoachList
{
	private ArrayList<HeadCoach> hcList = new ArrayList<>();
	private HashMap<Team, HeadCoach> hcAssignment = new HashMap<>();
	
	public HeadCoachList()
	{
		
	}
	
	public ArrayList<HeadCoach> getHcList()
	{
		return hcList;
	}
	
	public void setHcAssignment(HashMap<Team, HeadCoach> hcAssignment)
	{
		this.hcAssignment = hcAssignment;
	}
	
	public void setHcList(ArrayList<HeadCoach> hcList)
	{
		this.hcList = hcList;
	}
	
	public HashMap<Team, HeadCoach> getHcAssignment()
	{
		return hcAssignment;
	}
}
