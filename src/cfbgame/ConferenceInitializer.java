/**
 * ---------------------------------------------------------------------------
 * File name: ConferenceBuilder.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class ConferenceInitializer
{
	private String userDirectory = System.getProperty ("user.dir");
	
	private String[] confAssignmentFiles;
	
	private ConferenceLoader confLoader;
	private ConferenceList confList;
	private TeamList teamList;
	
	public ConferenceInitializer(ConferenceLoader confLoader, ConferenceList confList, TeamList teamList)
	{
		this.confLoader = confLoader;
		this.confList = confList;
		this.teamList = teamList;
	}
	
	public void setConferenceList(ConferenceList confList)
	{
		this.confList = confList;
	}
	
	public ConferenceList getConferenceList()
	{
		return confList;
	}

	public void buildConferences()
	{
		try
		{
			confLoader.loadFile ( );
			confLoader.populate ( );
			confList.setConfList (confLoader.getConferenceList ( ));
			exploreAssignmentFolder(userDirectory + "//definitions//conferences//");
			buildConfAssignments(teamList, confAssignmentFiles);
		}
		catch(FileNotFoundException e)
		{
			System.out.println ("Error: " + e.getMessage ( ));
		}
		catch(IOException e)
		{
			System.out.println("Error: " + e.getMessage ( ));
		}
	}
	
	private void buildConfAssignments(TeamList teamList, String[] assignmentFiles)
	{
		for(int i = 0; i < assignmentFiles.length; i++)
		{
			confLoader.loadConferenceAssignments (teamList.getTeamList ( ), assignmentFiles[i]);
		}
	}
	
	private void exploreAssignmentFolder(String filePath)
	{
		File f = new File(filePath);
		String[] confAssignmentFiles = f.list ( );
		for(int i = 0; i < confAssignmentFiles.length; i++)
		{
			confAssignmentFiles[i] = confAssignmentFiles[i].substring (0, confAssignmentFiles[i].lastIndexOf ('.'));
		}
		setConfAssignmentFiles(confAssignmentFiles);
	}
	
	private void setConfAssignmentFiles(String[] confAssignmentFiles)
	{
		this.confAssignmentFiles = confAssignmentFiles;
	}
}
