/**
 * ---------------------------------------------------------------------------
 * File name: ConferenceList.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 23, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.util.ArrayList;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 23, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public final class ConferenceList
{
	private ArrayList<Conference> confList = new ArrayList<Conference>();
	
	public ArrayList<Conference> getConfList()
	{
		return confList;
	}
	
	public void setConfList(ArrayList<Conference> confList)
	{
		this.confList = confList;
	}

	public Conference findConference(String triCode)
	{
		Conference conference = new Conference();
		for(int i = 0; i < confList.size ( ); i++)
		{
			if(triCode.equals(confList.get (i).getTriCode ( )))
			{
				conference = confList.get (i);
			}
		}
		return conference;
	}	
}
