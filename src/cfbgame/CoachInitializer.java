/**
 * ---------------------------------------------------------------------------
 * File name: CoachInitializer.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 26, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 26, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class CoachInitializer
{
	private ArrayList<HeadCoach> hcList;
	private ArrayList<OffCoordinator> ocList;
	private ArrayList<DefCoordinator> dcList;
	private HashMap<Team, HeadCoach> hcAssignment;
	private HashMap<Team, OffCoordinator> ocAssignment;
	private HashMap<Team, DefCoordinator> dcAssignment;
	
	
	private TeamList teamList;
	private CoachLoader coachLoader;
	
	public CoachInitializer(CoachLoader coachLoader, TeamList teamList)
	{
		this.coachLoader = coachLoader;
		this.teamList = teamList;
	}
	
	public void buildCoaches()
	{
		try
		{
			coachLoader.loadFile ( );
			coachLoader.populate ( );
			coachLoader.setTeamList (teamList);
			hcList = new ArrayList<>(coachLoader.getHeadCoachList ( ));
			ocList = new ArrayList<>(coachLoader.getOcList ( ));
			dcList = new ArrayList<>(coachLoader.getDcList ( ));
			coachLoader.loadCoachAssignments ( );			
			hcAssignment = coachLoader.getHcAssignment ( );
			ocAssignment = coachLoader.getOcAssignment ( );
			dcAssignment = coachLoader.getDcAssignment ( );
		}
		catch(FileNotFoundException e)
		{
			System.out.println(e.getMessage ( ));
		}
		catch(IOException e)
		{
			System.out.println(e.getMessage ( ));
		}
	}
	
	public ArrayList<HeadCoach> getHcList()
	{
		return hcList;
	}
	
	public ArrayList<OffCoordinator> getOcList()
	{
		return ocList;
	}
	
	public ArrayList<DefCoordinator> getDcList()
	{
		return dcList;
	}
	
	public HashMap<Team, HeadCoach> getHcMap()
	{
		return hcAssignment;
	}
	
	public HashMap<Team, OffCoordinator> getOcMap()
	{
		return ocAssignment;
	}
	
	public HashMap<Team, DefCoordinator> getDcMap()
	{
		return dcAssignment;
	}
}
