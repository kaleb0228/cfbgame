/**
 * ---------------------------------------------------------------------------
 * File name: History.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course:  CSCI 1260
 * Creation Date: Nov 20, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

import java.text.DecimalFormat;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 20, 2020
 * <hr>
 * @author Kaleb Dippold
 */
public class History
{
	private int wins;
	private int losses;
	private int ties;
	private int confChamps;
	private int natChamps;
	private double winPercent;
	private String triCode;
	DecimalFormat df = new DecimalFormat("0.000");
	
	public History()
	{
		wins = 0;
		losses = 0;
		ties = 0;
		confChamps = 0;
		natChamps = 0;
		triCode = "XXX";
	}
	
	public History(int wins, int losses, int ties, int confChamps, int natChamps, String triCode)
	{
		this.wins = wins;
		this.losses = losses;
		this.ties = ties;
		this.confChamps = confChamps;
		this.natChamps = natChamps;
		this.triCode = triCode;
		setWinPercent(wins, losses, ties);
	}
	
	public void setWinPercent(int wins, int losses, int ties)
	{
		int totalGames = (wins + losses + ties);
		double newWinPercent = (double)((wins + (ties * 0.5)) / (totalGames));
		this.winPercent = newWinPercent;
	}
	
	@Override
	public String toString()
	{
		return "Team: " + triCode + "\nWins: " + wins + "\nLosses: " + losses + "\nTies: " + ties + "\nConference Championships: " + confChamps + 
						"\nNational Championships: " + natChamps + "\nWin Percentage: " + df.format (winPercent);
	}
}
