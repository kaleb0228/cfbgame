/**
 * ---------------------------------------------------------------------------
 * File name: Main.java
 * Project name: CFB
 * ---------------------------------------------------------------------------
 * Creator's name and email: Kaleb Dippold, dippold@etsu.edu
 * Course: CSCI 1260
 * Creation Date: Nov 17, 2020
 * ---------------------------------------------------------------------------
 */

package cfbgame;

/**
 * Enter type purpose here
 *
 * <hr>
 * Date created: Nov 17, 2020
 * <hr>
 * 
 * @author Kaleb Dippold
 */
public class Main
{

	/**
	 * The application's entry point.
	 *
	 * <hr>
	 * Date created: Nov 17, 2020
	 *
	 * <hr>
	 * 
	 * @param args
	 */

	public static void main (String [ ] args) throws Exception
	{
		Initializer initializer = new Initializer();
		initializer.run ( );
	}

	/*	*//**
			 * 
			 * Initialize data of entities (teams, conferences, etc.) taken from definition files in /common folder.
			 *
			 * <hr>
			 * Date created: Nov 19, 2020
			 *
			 * <hr>
			 * 
			 * @param userDirectory
			 * @throws Exception
			 */
	/*
	 * public static void initializeData(String userDirectory)
	 * {
	 * TeamLoader tl = new TeamLoader (userDirectory + "\\definitions\\teams.txt");
	 * CoachLoader col = new CoachLoader(userDirectory + "\\definitions\\coaches.txt");
	 * ConferenceLoader cl = new ConferenceLoader(userDirectory + "\\definitions\\conferences.txt");
	 * try
	 * {
	 * tl.loadFile ( );
	 * tl.populate ( );
	 * col.loadFile ( );
	 * col.populate ( );
	 * cl.loadFile ( );
	 * cl.populate ( );
	 * assignConferences(cl);
	 * loadTeamHistory(userDirectory);
	 * loadCoachHistory(userDirectory);
	 * }
	 * catch(FileNotFoundException e)
	 * {
	 * JOptionPane.showMessageDialog (null, e.getMessage ( ));
	 * }
	 * catch(IOException e)
	 * {
	 * JOptionPane.showMessageDialog (null, e.getMessage ( ));
	 * }
	 * //System.out.println("Entity data loaded successfully.");
	 * //System.out.println("Loading conference assignments...");
	 * }
	 *//**
		 * 
		 * Calls on the ConferenceAssignmentLoader in order to assign teams to Multimap ConferenceAssignments.
		 *
		 * <hr>
		 * Date created: Nov 19, 2020
		 *
		 * <hr>
		 * 
		 * @param cl
		 * @throws Exception
		 *//*
			 * public static void assignConferences(ConferenceLoader cl) throws IOException
			 * {
			 * String[] conferences = {"ACC","B10","B12","P12","SEC","BE"};
			 * for(int i = 0; i < conferences.length; i++)
			 * {
			 * ConferenceAssignmentLoader cal = new ConferenceAssignmentLoader(conferences[i], cl);
			 * cal.loadFile ( );
			 * cal.populate ( );
			 * }
			 * }
			 * public static void loadTeamHistory(String userDirectory)
			 * {
			 * System.out.println("Initializing team history...");
			 * TeamHistoryLoader thl = new TeamHistoryLoader(userDirectory + "\\history\\teams\\");
			 * thl.exploreTeamFolder ();
			 * try
			 * {
			 * for(int i = 0; i < thl.getHistoryFiles ( ).length; i++)
			 * {
			 * System.out.println("Initializing history for team " + (i+1) + " out of " + thl.getHistoryFiles (
			 * ).length);
			 * thl.populate (thl.loadFile (thl.getHistoryFiles ( )[i]));
			 * System.out.println("Initialized history for team " + (i+1) + " successfully.");
			 * }
			 * }
			 * catch(FileNotFoundException e)
			 * {
			 * JOptionPane.showMessageDialog (null, e.getMessage ( ));
			 * }
			 * catch(IOException e)
			 * {
			 * JOptionPane.showMessageDialog (null, e.getMessage ( ));
			 * }
			 * System.out.println("Team history initialization successful.");
			 * }
			 * public static void loadCoachHistory(String userDirectory)
			 * {
			 * System.out.println("Initializing coach history...");
			 * CoachHistoryLoader ch = new CoachHistoryLoader (userDirectory + "\\history\\coaches\\");
			 * ch.exploreCoachesFolder ( );
			 * try
			 * {
			 * for(int i = 0; i < ch.getHistoryFolder ( ).length; i++)
			 * {
			 * System.out.println("Initializing history for coach " + (i+1) + " out of " + ch.getHistoryFolder (
			 * ).length);
			 * ch.populate (ch.loadFile (ch.getHistoryFolder ( )[i]));
			 * System.out.println("Initialized history for coach " + (i+1) + " successfully.");
			 * }
			 * }
			 * catch(FileNotFoundException e)
			 * {
			 * JOptionPane.showMessageDialog (null, e.getMessage ( ));
			 * }
			 * catch(IOException e)
			 * {
			 * JOptionPane.showMessageDialog (null, e.getMessage ( ));
			 * }
			 * finally
			 * {
			 * System.out.println("Coach history initialization successful.");
			 * }
			 * }
			 */
}
