#Year|Team|Wins|Losses|Ties|ConfChamps|NatChamps
2008|CLEM|4|3|0|0|0
2009|CLEM|9|5|0|0|0
2010|CLEM|6|7|0|0|0
2011|CLEM|10|4|0|1|0
2012|CLEM|11|2|0|0|0
2013|CLEM|11|2|0|0|0
2014|CLEM|10|3|0|0|0
2015|CLEM|14|1|0|1|0
2016|CLEM|14|1|0|1|1
2017|CLEM|12|2|0|1|0
2018|CLEM|15|0|0|1|1
2019|CLEM|14|1|0|1|0